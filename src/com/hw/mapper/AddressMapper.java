package com.hw.mapper;

import java.util.List;

import com.hw.entity.Address;

public interface AddressMapper {
	/**
	 * 查询所有地址
	 */
	List<Address> addressAll(Address a);
	/**
	 * 查询要修改的一条地址进行回显
	 */
	List<Address> addressByaddressId(Address a);
	/**
	 *获取总条数
	 */
	int addressCount(Address a);
	
	/**
	 * 添加
	 */
	int addressAdd(Address a);
	/**
	 * 添加信息
	 */
	int addressAdds(Address a);
	
	/**
	 * 修改
	 */
	int addressUpdate(Address a);
	
	/**
	 * 用户修改地址
	 */
	int addressUpdateq(Address a);
	
	/**
	 * 删除
	 */
	int addressDelete(int address_id);
}
