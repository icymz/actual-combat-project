package com.hw.mapper;

import com.hw.entity.AdminUser;

public interface AdminUserMapper {
	AdminUser getAdminUser(String uName);
}
