package com.hw.mapper;

import com.hw.entity.Shopcar;

public interface ShopcarMapper {
	/**
	 * 添加商品到购物车
	 * @param s
	 * @return
	 */
	int ShopAdd(Shopcar s);
	/**
	 * 修改购物车信息
	 * @param s
	 * @return
	 */
	int ShopUpdate(Shopcar s);
	/**
	 * 删除一条商品
	 * @param s
	 * @return
	 */
	int ShopDelete(Shopcar s);
	/**
	 * 清空购物车
	 * @param s
	 * @return
	 */
	int ShopDeletes(Shopcar s);
	
	
}
