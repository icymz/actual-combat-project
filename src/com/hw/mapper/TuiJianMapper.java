package com.hw.mapper;

import java.util.List;

import com.hw.entity.TuiJian;

public interface TuiJianMapper {
	
	/**
	 * 推荐表添加
	 * @param tj
	 * @return
	 */
	int tuiJianAdd(TuiJian tj);
	/**
	 * 推荐表修改
	 * @param tj
	 * @return
	 */
	int tuiJianUpdate(TuiJian tj);
	/**
	 * 推荐表删除
	 * @param tj
	 * @return
	 */
	int tuiJianDelete(TuiJian tj);
}
