package com.hw.mapper;

import java.util.List;

import com.hw.entity.Order_View;

public interface Order_ViewMapper {
	/**
	 * 多条件分页查询
	 */
	List<Order_View> order_ViewAll(Order_View o);
	/**
	 * 前台分页查询
	 */
	List<Order_View> order_ViewAllBypage(Order_View o);
	/**
	 * 显示
	 */
	List<Order_View> order_ViewAlls(Order_View o);
	/**
	 * 根据id查一条
	 * @param o
	 * @return
	 */
	List<Order_View> order_ViewAllById(Order_View o);
	
	/**
	 * 待收货,代发货,待付款...
	 */
	List<Order_View> order_ViewAllss(Order_View o);
	/**
	 * 获取总条数
	 */
	int order_ViewCount(Order_View o);
}
