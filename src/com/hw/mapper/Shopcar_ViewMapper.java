package com.hw.mapper;

import java.util.List;

import com.hw.entity.Shopcar_View;

public interface Shopcar_ViewMapper {
	/**
	 * 查询所有购物车信息
	 * @return
	 */
	List<Shopcar_View> Shopcar_ViewAll(Shopcar_View sv);
	/**
	 * 查询总条数
	 * @param sc
	 * @return
	 */
	int Shopcar_ViewAllCount(Shopcar_View sc);
	/**
	 * 查询该商品在不在购物车中
	 */
	List<Shopcar_View> Shopcar_ViewByProductId(Shopcar_View sv);
}
