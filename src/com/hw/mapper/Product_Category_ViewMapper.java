package com.hw.mapper;

import java.util.List;

import com.hw.entity.Product_Category_View;

public interface Product_Category_ViewMapper {
	/**
	 * 多条件分页查询
	 * @param pview
	 * @return
	 */
	List<Product_Category_View> product_Category_ViewAll(Product_Category_View pview);
	/**
	 * 获取总条数
	 */
	int product_Category_ViewCount(Product_Category_View pview);
	/**
	 *添加 
	 */
	int  product_Category_ViewAdd(Product_Category_View pview);
	
	/**
	 * 修改
	 */
	int product_Category_ViewUpdate(Product_Category_View pview);
	
	/**
	 * 删除
	 */
	int product_Category_ViewDelete(int product_id);
	
	/**
	 * 根据id批量删除
	 */
	int product_Category_ViewDeleteIds(List<Integer> product_ids);
	/**
	 * 多条件分页查询
	 * @param pview
	 * @return
	 */
	List<Product_Category_View> product_Category_ViewAlls(Product_Category_View pview);
	/**
	 * 获取总条数
	 */
	int product_Category_ViewCounts(Product_Category_View pview);
	/**
	 * 多条件分页查询
	 * @param pview
	 * @return
	 */
	List<Product_Category_View> product_Category_ViewAllss(Product_Category_View pview);
}
