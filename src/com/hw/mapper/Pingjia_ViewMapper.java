package com.hw.mapper;

import java.util.List;

import com.hw.entity.Pingjia_View;

public interface Pingjia_ViewMapper {
	/**
	 * 查询
	 * @param pingjia_View
	 * @return
	 */
	List<Pingjia_View> pingjia_ViewAll(Pingjia_View pingjia_View);
	
	/**
	 * 查询该商品所有的评价
	 * @param pingjia_View
	 * @return
	 */
	List<Pingjia_View> pingjia_ViewByproduct_id(Pingjia_View pingjia_View);
	
	/**
	 * 查询分页数据
	 */
	int pingjia_ViewByproduct_idCount(Pingjia_View pingjia_View);
}
