package com.hw.service;

import java.util.List;

import com.hw.entity.ProductType;

public interface ProductTypeService {
	/**
	 * 查询所有分类
	 * @param pt
	 * @return
	 */
	List<ProductType> productTypeAll(ProductType pt);
	
	/**
	 * 数组形式
	 * @return
	 */
	List<ProductType> productTypeType();
	/**
	 * 分类总数
	 * @param pt
	 * @return
	 */
	int productTypeCount(ProductType pt);
	/**
	 * 分类添加
	 * @param pt
	 * @return
	 */
	int productTypeaAdd(ProductType pt);
	/**
	 * 分类修改
	 * @param pt
	 * @return
	 */
	int productTypeUpdate(ProductType pt);
	/**
	 * 分类删除
	 * @param pt
	 * @return
	 */
	int productTypeDelete(ProductType pt);
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	int productTypeDeletes(List<Integer> ids);
	
}
