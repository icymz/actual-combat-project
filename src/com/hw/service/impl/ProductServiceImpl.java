package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Product;
import com.hw.mapper.ProductMapper;
import com.hw.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	
	@Autowired
	private ProductMapper pMapper;
	
	
	
	@Override
	public int productAdd(Product p) {
		// TODO Auto-generated method stub
		return pMapper.productAdd(p);
	}

	@Override
	public int productUpdate(Product p) {
		// TODO Auto-generated method stub
		return pMapper.productUpdate(p);
	}

	@Override
	public int productDelete(int product_id) {
		// TODO Auto-generated method stub
		return pMapper.productDelete(product_id);
	}

	@Override
	public int productDeleteIds(List<Integer> product_ids) {
		// TODO Auto-generated method stub
		return pMapper.productDeleteIds(product_ids);
	}

	@Override
	public int productIspub(Product p) {
		// TODO Auto-generated method stub
		return pMapper.productIspub(p);
	}

}
