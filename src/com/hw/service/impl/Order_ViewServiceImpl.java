package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Order_View;
import com.hw.mapper.Order_ViewMapper;
import com.hw.service.Order_ViewService;

@Service
public class Order_ViewServiceImpl implements Order_ViewService{

	@Autowired
	private Order_ViewMapper ovMapper;
	
	
	@Override
	public List<Order_View> order_ViewAll(Order_View o) {
		// TODO Auto-generated method stub
		return ovMapper.order_ViewAll(o);
	}


	@Override
	public int order_ViewCount(Order_View o) {
		// TODO Auto-generated method stub
		return ovMapper.order_ViewCount(o);
	}


	@Override
	public List<Order_View> order_ViewAlls(Order_View o) {
		// TODO Auto-generated method stub
		return ovMapper.order_ViewAlls(o);
	}


	@Override
	public List<Order_View> order_ViewAllById(Order_View o) {
		// TODO Auto-generated method stub
		return ovMapper.order_ViewAllById(o);
	}


	@Override
	public List<Order_View> order_ViewAllBypage(Order_View o) {
		// TODO Auto-generated method stub
		return ovMapper.order_ViewAllBypage(o);
	}

}
