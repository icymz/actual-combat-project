package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Product_Category_View;
import com.hw.mapper.Product_Category_ViewMapper;
import com.hw.service.Product_Category_ViewService;
@Service
public class Product_Category_ViewServiceImpl implements Product_Category_ViewService{

	@Autowired
	private Product_Category_ViewMapper pcViewMapper;
	
	@Override
	public List<Product_Category_View> product_Category_ViewAll(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewAll(pview);
	}

	@Override
	public int product_Category_ViewCount(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewCount(pview);
	}

	@Override
	public int product_Category_ViewAdd(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewAdd(pview);
	}

	@Override
	public int product_Category_ViewUpdate(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewUpdate(pview);
	}

	@Override
	public int product_Category_ViewDelete(int product_id) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewDelete(product_id);
	}

	@Override
	public int product_Category_ViewDeleteIds(List<Integer> product_ids) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewDeleteIds(product_ids);
	}

	@Override
	public List<Product_Category_View> product_Category_ViewAlls(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewAlls(pview);
	}

	@Override
	public int product_Category_ViewCounts(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewCounts(pview);
	}

	@Override
	public List<Product_Category_View> product_Category_ViewAllss(Product_Category_View pview) {
		// TODO Auto-generated method stub
		return pcViewMapper.product_Category_ViewAllss(pview);
	}

}
