package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.TuiJian_view;
import com.hw.mapper.TuiJian_viewMapper;
import com.hw.service.TuiJian_viewService;

@Service
public class TuiJian_viewServiceImpl implements TuiJian_viewService{

	@Autowired
	private TuiJian_viewMapper tvMapper;
	
	@Override
	public List<TuiJian_view> tuiJian_viewAll(TuiJian_view tv) {
		// TODO Auto-generated method stub
		return tvMapper.tuiJian_viewAll(tv);
	}

	@Override
	public int getCount(TuiJian_view tv) {
		// TODO Auto-generated method stub
		return tvMapper.getCount(tv);
	}

	@Override
	public List<TuiJian_view> tuiJian_viewAlls(TuiJian_view tv) {
		// TODO Auto-generated method stub
		return tvMapper.tuiJian_viewAlls(tv);
	}

}
