package com.hw.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Pingjia;
import com.hw.mapper.PingjiaMapper;
import com.hw.service.PingjiaService;
@Service
public class PingjiaServiceImpl implements PingjiaService{

	@Autowired
	private PingjiaMapper pMapper;
	
	
	@Override
	public int pingjiaAdd(Pingjia p) {
		// TODO Auto-generated method stub
		return pMapper.pingjiaAdd(p);
	}

}
