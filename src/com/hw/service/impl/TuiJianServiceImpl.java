package com.hw.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.TuiJian;
import com.hw.mapper.TuiJianMapper;
import com.hw.service.TuiJianService;

@Service
public class TuiJianServiceImpl implements TuiJianService{

	@Autowired
	private TuiJianMapper tMapper;
	
	@Override
	public int tuiJianAdd(TuiJian tj) {
		// TODO Auto-generated method stub
		return tMapper.tuiJianAdd(tj);
	}

	@Override
	public int tuiJianUpdate(TuiJian tj) {
		// TODO Auto-generated method stub
		return tMapper.tuiJianUpdate(tj);
	}

	@Override
	public int tuiJianDelete(TuiJian tj) {
		// TODO Auto-generated method stub
		return tMapper.tuiJianDelete(tj);
	}

}
