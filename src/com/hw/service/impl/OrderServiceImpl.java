package com.hw.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Order;
import com.hw.mapper.OrderMapper;
import com.hw.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{

	@Autowired
	private OrderMapper oMapper;
	
	
	@Override
	public int orderAdd(Order o) {
		// TODO Auto-generated method stub
		return oMapper.orderAdd(o);
	}

	@Override
	public int orderUpdate(Order o) {
		// TODO Auto-generated method stub
		return oMapper.orderUpdate(o);
	}

	@Override
	public int orderDelete(int order_id) {
		// TODO Auto-generated method stub
		return oMapper.orderDelete(order_id);
	}

	@Override
	public int orderUpdates(Order o) {
		// TODO Auto-generated method stub
		return oMapper.orderUpdates(o);
	}

	@Override
	public int orderUpdateBy1(Order o) {
		// TODO Auto-generated method stub
		return oMapper.orderUpdateBy1(o);
	}

	@Override
	public int orderAdds(Order o) {
		// TODO Auto-generated method stub
		return oMapper.orderAdds(o);
	}

}
