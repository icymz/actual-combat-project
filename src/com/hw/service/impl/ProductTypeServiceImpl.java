package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.ProductType;
import com.hw.mapper.ProductTypeMapper;
import com.hw.service.ProductTypeService;

@Service
public class ProductTypeServiceImpl implements ProductTypeService{

	@Autowired
	private ProductTypeMapper ptMapper;
	
	@Override
	public List<ProductType> productTypeAll(ProductType pt) {
		// TODO Auto-generated method stub
		return ptMapper.productTypeAll(pt);
	}

	@Override
	public int productTypeCount(ProductType pt) {
		// TODO Auto-generated method stub
		return ptMapper.productTypeCount(pt);
	}

	@Override
	public int productTypeaAdd(ProductType pt) {
		// TODO Auto-generated method stub
		return ptMapper.productTypeaAdd(pt);
	}

	@Override
	public int productTypeUpdate(ProductType pt) {
		// TODO Auto-generated method stub
		return ptMapper.productTypeUpdate(pt);
	}

	@Override
	public int productTypeDelete(ProductType pt) {
		// TODO Auto-generated method stub
		return ptMapper.productTypeDelete(pt);
	}

	@Override
	public int productTypeDeletes(List<Integer> ids) {
		// TODO Auto-generated method stub
		return ptMapper.productTypeDeletes(ids);
	}

	@Override
	public List<ProductType> productTypeType() {
		// TODO Auto-generated method stub
		return ptMapper.productTypeType();
	}

}
