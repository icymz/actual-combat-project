package com.hw.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Shopcar;
import com.hw.mapper.ShopcarMapper;
import com.hw.service.ShopcarService;

@Service
public class ShopcarServiceImpl implements ShopcarService{

	@Autowired
	private ShopcarMapper sMapper;
	
	@Override
	public int ShopAdd(Shopcar s) {
		// TODO Auto-generated method stub
		return sMapper.ShopAdd(s);
	}

	@Override
	public int ShopUpdate(Shopcar s) {
		// TODO Auto-generated method stub
		return sMapper.ShopUpdate(s);
	}

	@Override
	public int ShopDelete(Shopcar s) {
		// TODO Auto-generated method stub
		return sMapper.ShopDelete(s);
	}

	@Override
	public int ShopDeletes(Shopcar s) {
		// TODO Auto-generated method stub
		return sMapper.ShopDeletes(s);
	}

}
