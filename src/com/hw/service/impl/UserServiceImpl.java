package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.User;
import com.hw.mapper.UserMapper;
import com.hw.service.UserService;
@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserMapper uMapper;
	
	
	@Override
	public List<User> userAll(User u) {
		// TODO Auto-generated method stub
		return uMapper.userAll(u);
	}

	@Override
	public int userAdd(User u) {
		// TODO Auto-generated method stub
		return uMapper.userAdd(u);
	}

	@Override
	public int userUpdate(User u) {
		// TODO Auto-generated method stub
		return uMapper.userUpdate(u);
	}

	@Override
	public int userDelete(int user_id) {
		// TODO Auto-generated method stub
		return uMapper.userDelete(user_id);
	}

	@Override
	public User login(User u) {
		// TODO Auto-generated method stub
		return uMapper.login(u);
	}

	@Override
	public int updateImg(User u) {
		// TODO Auto-generated method stub
		return uMapper.updateImg(u);
	}

	@Override
	public int userPassword(User u) {
		// TODO Auto-generated method stub
		return uMapper.userPassword(u);
	}

	@Override
	public int userAddq(User u) {
		// TODO Auto-generated method stub
		return uMapper.userAddq(u);
	}

	@Override
	public List<User> userpwd(User u) {
		// TODO Auto-generated method stub
		return uMapper.userpwd(u);
	}

	

}
