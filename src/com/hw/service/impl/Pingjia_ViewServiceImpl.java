package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Pingjia_View;
import com.hw.mapper.Pingjia_ViewMapper;
import com.hw.service.Pingjia_ViewService;
@Service
public class Pingjia_ViewServiceImpl implements Pingjia_ViewService{

	@Autowired
	private Pingjia_ViewMapper pingjia_ViewMapper;
	
	
	@Override
	public List<Pingjia_View> pingjia_ViewAll(Pingjia_View pingjia_View) {
		// TODO Auto-generated method stub
		return pingjia_ViewMapper.pingjia_ViewAll(pingjia_View);
	}


	@Override
	public List<Pingjia_View> pingjia_ViewByproduct_id(Pingjia_View pingjia_View) {
		// TODO Auto-generated method stub
		return pingjia_ViewMapper.pingjia_ViewByproduct_id(pingjia_View);
	}


	@Override
	public int pingjia_ViewByproduct_idCount(Pingjia_View pingjia_View) {
		// TODO Auto-generated method stub
		return pingjia_ViewMapper.pingjia_ViewByproduct_idCount(pingjia_View);
	}

}
