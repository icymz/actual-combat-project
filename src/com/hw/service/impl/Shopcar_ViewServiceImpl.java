package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Shopcar_View;
import com.hw.mapper.Shopcar_ViewMapper;
import com.hw.service.Shopcar_ViewService;

@Service
public class Shopcar_ViewServiceImpl implements Shopcar_ViewService{

	@Autowired
	private Shopcar_ViewMapper SVMapper;
	


	@Override
	public List<Shopcar_View> Shopcar_ViewAll(Shopcar_View sv) {
		// TODO Auto-generated method stub
		return SVMapper.Shopcar_ViewAll(sv);
	}



	@Override
	public int Shopcar_ViewAllCount(Shopcar_View sc) {
		// TODO Auto-generated method stub
		return SVMapper.Shopcar_ViewAllCount(sc);
	}



	@Override
	public List<Shopcar_View> Shopcar_ViewByProductId(Shopcar_View sv) {
		// TODO Auto-generated method stub
		return SVMapper.Shopcar_ViewByProductId(sv);
	}







}
