package com.hw.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.AdminUser;
import com.hw.mapper.AdminUserMapper;
import com.hw.service.AdminUserService;
@Service
public class AdminUserServiceImpl implements AdminUserService{
	@Autowired
	AdminUserMapper adminUserMapper;
	@Override
	public AdminUser findAdminUser(String uName) {
		// TODO Auto-generated method stub
		return adminUserMapper.getAdminUser(uName);
	}

}
