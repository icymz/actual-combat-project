package com.hw.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hw.entity.Address;
import com.hw.mapper.AddressMapper;
import com.hw.service.AddressService;
@Service
public class AddressServiceImpl implements AddressService{

	@Autowired
	private AddressMapper aMapper;
	
	
	@Override
	public List<Address> addressAll(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressAll(a);
	}

	@Override
	public int addressCount(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressCount(a);
	}

	@Override
	public int addressAdd(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressAdd(a);
	}

	@Override
	public int addressUpdate(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressUpdate(a);
	}

	@Override
	public int addressDelete(int address_id) {
		// TODO Auto-generated method stub
		return aMapper.addressDelete(address_id);
	}

	@Override
	public int addressAdds(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressAdds(a);
	}

	@Override
	public List<Address> addressByaddressId(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressByaddressId(a);
	}

	@Override
	public int addressUpdateq(Address a) {
		// TODO Auto-generated method stub
		return aMapper.addressUpdateq(a);
	}

}
