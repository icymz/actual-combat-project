package com.hw.service;

import java.util.List;

import com.hw.entity.TuiJian_view;

public interface TuiJian_viewService {
	/**
	 * 视图推荐表查询所有
	 * @param tv
	 * @return
	 */
	List<TuiJian_view> tuiJian_viewAll(TuiJian_view tv);
	/**
	 * 00.00
	 */
	List<TuiJian_view> tuiJian_viewAlls(TuiJian_view tv);
	/**
	 * 计数
	 * @param tv
	 * @return
	 */
	int getCount(TuiJian_view tv);
}
