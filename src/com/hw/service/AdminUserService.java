package com.hw.service;

import com.hw.entity.AdminUser;

public interface AdminUserService {
	AdminUser findAdminUser(String uName);
}
