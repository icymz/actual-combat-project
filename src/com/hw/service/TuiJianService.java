package com.hw.service;

import com.hw.entity.TuiJian;

public interface TuiJianService {
	/**
	 * 推荐表添加
	 * @param tj
	 * @return
	 */
	int tuiJianAdd(TuiJian tj);
	/**
	 * 推荐表修改
	 * @param tj
	 * @return
	 */
	int tuiJianUpdate(TuiJian tj);
	/**
	 * 推荐表删除
	 * @param tj
	 * @return
	 */
	int tuiJianDelete(TuiJian tj);
}
