package com.hw.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class BaseServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        
        //閼惧嘲褰囬崣鍌涙殶閺傝纭堕崥锟�?
        String methodName = request.getParameter("method");
        Class c = this.getClass();//閼惧嘲褰囪ぐ鎾冲缁拷
        Method method = null;
        try {
        	//闁俺绻冮崣鍌涙殶閸氬秷骞忛崣鏍ь嚠鎼存棁顕Ч淇縠rvlet娑擃厾娈戦弬瑙勭�?
            method = c.getMethod(methodName, HttpServletRequest.class,HttpServletResponse.class);
        } catch (Exception e){
            throw new RuntimeException("閹劏顩︾拫鍐暏閻ㄥ嫭鏌熷▔鏇窗" + methodName +
                    "(HttpServletRequest,HttpServletResponse)閿涘苯鐣犳稉宥呯摠閸︻煉绱�");
        }
        try{
        	//鐠嬪啰鏁ら弬瑙勭《婢跺嫮鎮婄拠閿嬬湴
            method.invoke(this,request, response);
            //System.out.println(methodName+"閺傝纭堕幍褑顢戞禍锟�");
        }catch(Exception e){
            System.out.println("閹劏鐨熼悽銊ф畱閺傝纭堕敍锟�" + methodName + ",閵嗭拷鐎瑰啫鍞撮柈銊�?閸戣桨绨�?�鍌氱埗閿涳拷");
            throw new RuntimeException(e); 
        }
		
	}

}
