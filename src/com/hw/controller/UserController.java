package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.User;
import com.hw.service.UserService;
import com.hw.util.Result;

@Controller
public class UserController {
	
	@Autowired
	private UserService uService;
	/**
	 * 多条条件分页查询
	 * @param u
	 * @return
	 */
	@RequestMapping("userAll")
	@ResponseBody
	public Object userAll(User u) {
		return uService.userAll(u);
		
	}
	@RequestMapping("login")
	@ResponseBody
	public Object login(User u) {
		User user = uService.login(u);
		if(u.getUser_loginname()!=null){
			if(u.getUser_pwd().equals(user.getUser_pwd())){
				return Result.toClient("0", "", 0 , user);
			} else {
				return Result.toClient("1", "密码错误");
			}
		} else {
			return Result.toClient("2", "账号错误");
		}

	}
	
	/**
	 * 多条条件分页查询
	 * @param u
	 * @return
	 */
	@RequestMapping("userpwd")
	@ResponseBody
	public Object userpwd(User u) {
		return uService.userpwd(u);
		
	}
	
	/**
	 * 添加
	 * @param u
	 * @return
	 */
	@RequestMapping("userAdd")
	@ResponseBody
	public Object userAdd(User u) {
		int add = uService.userAdd(u);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
	}
	/**
	 * 添加
	 * @param u
	 * @return
	 */
	@RequestMapping("userAddq")
	@ResponseBody
	public Object userAddq(User u) {
		int add = uService.userAdd(u);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
	}
	/**
	 * 修改
	 * @param u
	 * @return
	 */
	@RequestMapping("userUpdate")
	@ResponseBody
	public Object userUpdate(User u) {
		int update = uService.userUpdate(u);
		if (update>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
	
	/**
	 * 修改头像
	 * @param u
	 * @return
	 */
	@RequestMapping("updateImg")
	@ResponseBody
	public Object userImg(User u) {
		int update = uService.updateImg(u);
		if (update>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
	
	/**
	 * 删除
	 * @param u
	 * @return
	 */
	@RequestMapping("userDelete")
	@ResponseBody
	public Object userDelete(int user_id) {
		int delete = uService.userDelete(user_id);
		if (delete>0) {
			return Result.toClient("0", "删除成功");
		} else {
			return Result.toClient("1", "删除失败");
		}
	}
	/**
	 * 修改密码
	 * @param u
	 * @return
	 */
	@RequestMapping("userPassword")
	@ResponseBody
	public Object userPassword(User u) {
		int userPassword = uService.userPassword(u);
		if (userPassword>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
}
