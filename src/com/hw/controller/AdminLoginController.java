package com.hw.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.AdminUser;
import com.hw.service.AdminUserService;

@Controller
public class AdminLoginController {
		@Autowired
		AdminUserService adminUserService;
		
		
	
		//后台登录
		@RequestMapping("adminLoginUser")
		@ResponseBody
		public Object adminLoginUser(AdminUser adminUser,HttpSession session) {
			AdminUser adus= adminUserService.findAdminUser(adminUser.getUserName());
			if(adus!=null) {//账号正确
				System.out.println(adminUser.getUserName());
				if(adus.getPassword().equals(adminUser.getPassword())) {
					session.setAttribute("loginUser",adminUser);
					return 0;
				}
				
			}
			return 1;
		}
		//获取session中的值
		@RequestMapping("getAdminLoginUser")
		@ResponseBody
		public Object getAdminLoginUser(HttpSession session) {
			AdminUser admin=(AdminUser) session.getAttribute("loginUser");
			return admin;
		}
		
		//清空session中的值
		@RequestMapping("delAdminLoginUser")
		@ResponseBody
		public Object delAdminLoginUser(AdminUser adminUser,HttpSession session) {
			if(adminUser!=null) {
				session.invalidate();
				return 0;
			}
			return 1;
		}
}
