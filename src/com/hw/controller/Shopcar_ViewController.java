package com.hw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Shopcar_View;
import com.hw.service.Shopcar_ViewService;

@Controller
public class Shopcar_ViewController {
	
	@Autowired
	private Shopcar_ViewService SVService;
	
	@RequestMapping("Shopcar_ViewAll")
	@ResponseBody
	public Object Shopcar_ViewAll(Shopcar_View sv) {
		return SVService.Shopcar_ViewAll(sv);
	}
	
	@RequestMapping("Shopcar_ViewAllCount")
	@ResponseBody
	public Object Shopcar_ViewAllCount(Shopcar_View sv) {
		return SVService.Shopcar_ViewAllCount(sv);
	}
	
	
	@RequestMapping("Shopcar_ViewByProductId")
	@ResponseBody
	public Object Shopcar_ViewByProductId(Shopcar_View sv) {
		return SVService.Shopcar_ViewByProductId(sv);
	}
	
}
