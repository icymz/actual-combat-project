package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Product_Category_View;
import com.hw.service.Product_Category_ViewService;
import com.hw.util.Result;


@Controller
public class Product_Category_ViewController {

	@Autowired
	private Product_Category_ViewService pcViewService;
	
	/**
	 * 多条件分页查询
	 * @param pview
	 * @return
	 */
	@RequestMapping("product_Category_ViewAll")
	@ResponseBody
	public Object product_Category_ViewAll(Product_Category_View pview){
		pview.setPage((pview.getPage()-1)*pview.getLimit());
		
		List<Product_Category_View> list = pcViewService.product_Category_ViewAll(pview);
		int count = pcViewService.product_Category_ViewCount(pview);
		if (list.size()>0) {
			return Result.toClient("0", "", count, list);
		} else {
			return Result.toClient("1", "失败");
		}
		
	}
	
	/**
	 *添加
	 * @param pview
	 * @return
	 */
	@RequestMapping("product_Category_ViewAdd")
	@ResponseBody
	public Object product_Category_ViewAdd(Product_Category_View pview){
		
		int add = pcViewService.product_Category_ViewAdd(pview);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
		
	}
	/**
	 *修改
	 * @param pview
	 * @return
	 */
	@RequestMapping("product_Category_ViewUpdate")
	@ResponseBody
	public Object product_Category_ViewUpdate(Product_Category_View pview){
		
		int update = pcViewService.product_Category_ViewUpdate(pview);
		if (update>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
		
	}
	/**
	 *删除
	 * @param pview
	 * @return
	 */
	@RequestMapping("product_Category_ViewDelete")
	@ResponseBody
	public Object product_Category_ViewDelete(int product_id){
		
		int delete = pcViewService.product_Category_ViewDelete(product_id);
		if (delete>0) {
			return Result.toClient("0", "删除成功");
		} else {
			return Result.toClient("1", "删除失败");
		}
		
	}
	/**
	 * 多条件分页查询
	 * @param pview
	 * @return
	 */
	@RequestMapping("product_Category_ViewAlls")
	@ResponseBody
	public Object product_Category_ViewAlls(Product_Category_View pview){
		pview.setPage((pview.getPage()-1)*pview.getLimit());
		//return pcViewService.product_Category_ViewAlls(pview);
		List<Product_Category_View> list = pcViewService.product_Category_ViewAlls(pview);
		int count = pcViewService.product_Category_ViewCounts(pview);
		if (list.size()>0) {
			return Result.toClient("0", "", count, list);
		} else {
			return Result.toClient("1", "失败");
		}
	}
	
	/**
	 * 多条件分页查询
	 * @param pview
	 * @return
	 */
	@RequestMapping("product_Category_ViewAllss")
	@ResponseBody
	public Object product_Category_ViewAllss(Product_Category_View pview){
		return pcViewService.product_Category_ViewAllss(pview);
		
	}
}
