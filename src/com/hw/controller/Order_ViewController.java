package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Order_View;
import com.hw.service.Order_ViewService;
import com.hw.util.Result;

@Controller
public class Order_ViewController {

	@Autowired
	private Order_ViewService ovService;
	/**
	 * 多条件分页查询
	 * @param o
	 * @return
	 */
	@RequestMapping("order_ViewAll")
	@ResponseBody
	public Object order_ViewAll(Order_View o){
		o.setPage((o.getPage()-1)*o.getLimit());
		
		List<Order_View> list = ovService.order_ViewAll(o);
		int count = ovService.order_ViewCount(o);
		if (list.size()>0) {
			return Result.toClient("0", "", count, list);
		} else {
			return Result.toClient("1", "失败");
		}
		
	}
	@RequestMapping("order_Viewq")
	@ResponseBody
	public Object order_Viewq(Order_View o){
		
		return ovService.order_ViewAll(o);
	
	}
	
	@RequestMapping("order_ViewAllBypage")
	@ResponseBody
	public Object order_ViewAllBypage(Order_View o){
		o.setPage((o.getPage()-1)*o.getLimit());
		
		return  ovService.order_ViewAllBypage(o);
		
		
	}
	
	
	@RequestMapping("order_ViewAllById")
	@ResponseBody
	public Object order_ViewAllById(Order_View o){
		
		return ovService.order_ViewAllById(o);
	
	}
	
	@RequestMapping("order_ViewAlls")
	@ResponseBody
	public Object order_ViewAlls(Order_View o){
		
		return ovService.order_ViewAlls(o);
		
		
	}
}
