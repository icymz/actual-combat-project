package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.TuiJian_view;
import com.hw.service.TuiJian_viewService;
import com.hw.util.Result;

@Controller
public class TuiJian_viewController {

	@Autowired
	private TuiJian_viewService tvService;
	
	@RequestMapping("tuiJian_viewAll")
	@ResponseBody
	public Object tuiJian_viewAll(TuiJian_view tv) {
		tv.setPage((tv.getPage()-1)*tv.getLimit());
		List<TuiJian_view> list = tvService.tuiJian_viewAll(tv);
		int count = tvService.getCount(tv);
		if (list.size()>0) {
			return Result.toClient("0", "", count, list);
		} else {
			return Result.toClient("1", "��ѯʧ��");
		}
		
	}
	@RequestMapping("tuiJian_viewAlls")
	@ResponseBody
	public Object tuiJian_viewAlls(TuiJian_view tv) {
		return tvService.tuiJian_viewAlls(tv);
	}
}
