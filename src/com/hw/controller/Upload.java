package com.hw.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hw.util.Result;
import com.jspsmart.upload.SmartUpload;
import com.jspsmart.upload.SmartUploadException;

/**
 * Servlet implementation class Upload
 */
public class Upload extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		//1创建SmartUpload对象
		SmartUpload smart=new SmartUpload();
		//2修改编码格式防止中文乱码
		smart.setCharset("UTF-8");
		//3初始化
		smart.initialize(getServletConfig(), request, response);
		
		try {
			//4上传
			smart.upload();
			//5保存
			smart.save("/static/admin/images");//上传的路径，该路径必须是存在的
			//6获取上传图片的文件名
			String filesname=smart.getFiles().getFile(0).getFileName();
			out.print(Result.toClient("0", "上传成功", 0, filesname));
		} catch (SmartUploadException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			out.print(Result.toClient("0", "上传成功"));
		}
		
		out.flush();
		out.close();
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
