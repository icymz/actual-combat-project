package com.hw.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.hw.service.FileService;
	 @Controller
	 @RequestMapping("/save")
	 public class FileController {
	 
	     @Autowired
	     private FileService fileService;
	     
	     @RequestMapping("/saveImg")
	     @ResponseBody
	     public String saveImg(MultipartFile upload){
	    	 return fileService.upLoadFile(upload);
	     }
	 }
 
	

