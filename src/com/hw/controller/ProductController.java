package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Product;
import com.hw.service.ProductService;
import com.hw.util.Result;

@Controller
public class ProductController {

	@Autowired
	private ProductService pService;
	/**
	 * 添加
	 * @param p
	 * @return
	 */
	@RequestMapping("productAdd")
	@ResponseBody
	public Object productAdd(Product p){
		int add = pService.productAdd(p);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
	}
	/**
	 * 修改
	 * @param p
	 * @return
	 */
	@RequestMapping("productUpdate")
	@ResponseBody
	public Object productUpdate(Product p){
		int update = pService.productUpdate(p);
		if (update>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
	/**
	 * 上架下架
	 */
	@RequestMapping("productIspub")
	@ResponseBody
	public Object productIspub(Product p) {
		int ispub = pService.productIspub(p);
		if (ispub>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
	/**
	 * 删除
	 * @param product_id
	 * @return
	 */
	@RequestMapping("productDelete")
	@ResponseBody
	public Object productDelete(int product_id){
		int delete = pService.productDelete(product_id);
		if (delete>0) {
			return Result.toClient("0", "删除成功");
		} else {
			return Result.toClient("1", "删除失败");
		}
	}
	/**
	 * 批量删除
	 * @param product_ids
	 * @return
	 */
	@RequestMapping("productTypeDeletes")
	@ResponseBody
	public Object productDeleteIds(List<Integer> product_ids) {

		int deletes = pService.productDeleteIds(product_ids);
		if (deletes>0) {
			return Result.toClient("0", "批量删除成功");
		} else {
			return Result.toClient("1", "批量删除失败");
		}
	}
}
