package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Address;
import com.hw.service.AddressService;
import com.hw.util.Result;

@Controller
public class AddressController {

	@Autowired
	private AddressService aService;
	
	/**
	 * 查询所有地址
	 */
	@RequestMapping("addressAll")
	@ResponseBody
	public Object addressAll(Address a) {
		a.setPage((a.getPage()-1)*a.getLimit());
		
		List<Address> list = aService.addressAll(a);
		if (list.size()>0) {
			return Result.toClient("0", "", 0, list);
		} else {
			return Result.toClient("1", "失败");
		}
	}
	
	@RequestMapping("addressq")
	@ResponseBody
	public Object addressq(Address a) {

		return  aService.addressAll(a);
		
	}
	
	@RequestMapping("addressByaddressId")
	@ResponseBody
	public Object addressByaddressId(Address a) {

		return  aService.addressByaddressId(a);
		
	}
	
	/**
	 * 添加  (前台创建新地址)
	 */
	@RequestMapping("addressAdd")
	@ResponseBody
	public Object addressAdd(Address a) {
		
		int add = aService.addressAdd(a);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
	}
	/**
	 * 添加  (前台创建新地址)
	 */
	@RequestMapping("addressAdds")
	@ResponseBody
	public Object addressAdds(Address a) {
		
		int add = aService.addressAdds(a);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
	}
	/**
	 *修改
	 */
	@RequestMapping("addressUpdate")
	@ResponseBody
	public Object addressUpdate(Address a) {
		
		int update = aService.addressUpdate(a);
		if (update>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
	/**
	 *用户修改地址
	 */
	@RequestMapping("addressUpdateq")
	@ResponseBody
	public Object addressUpdateq(Address a) {
		
		int update = aService.addressUpdateq(a);
		if (update>0) {
			return Result.toClient("0", "修改成功");
		} else {
			return Result.toClient("1", "修改失败");
		}
	}
	/**
	 *删除
	 */
	@RequestMapping("addressDelete")
	@ResponseBody
	public Object addressDelete(int address_id) {
		
		int delete = aService.addressDelete(address_id);
		if (delete>0) {
			return Result.toClient("0", "删除成功");
		} else {
			return Result.toClient("1", "删除失败");
		}
	}
}
