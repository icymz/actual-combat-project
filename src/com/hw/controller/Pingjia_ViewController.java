package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Pingjia_View;
import com.hw.service.Pingjia_ViewService;
import com.hw.util.Result;

@Controller
public class Pingjia_ViewController {

	
	@Autowired
	private Pingjia_ViewService pingjia_ViewService;
	
	/**
	 * 查询
	 * @param pingjia_View
	 * @return
	 */
	@RequestMapping("pingjia_ViewAll")
	@ResponseBody
	public Object pingjia_ViewAll(Pingjia_View pingjia_View) {
		return pingjia_ViewService.pingjia_ViewAll(pingjia_View);
		
	}
	
	
	/**
	 * 查询该商品所有的评价
	 * @param pingjia_View
	 * @return
	 */
	@RequestMapping("pingjia_ViewByproduct_id")
	@ResponseBody
	public Object pingjia_ViewByproduct_id(Pingjia_View pingjia_View) {
		pingjia_View.setPage((pingjia_View.getPage()-1)*pingjia_View.getLimit());
		List<Pingjia_View> list = pingjia_ViewService.pingjia_ViewByproduct_id(pingjia_View);
		int count = pingjia_ViewService.pingjia_ViewByproduct_idCount(pingjia_View);
		if (list.size()>0) {
			return Result.toClient("0", "", count, list);
		} else {
			return Result.toClient("1", "查询失败");
		}
		
	}
	
	/**
	 * 查询该商品所有的评价数量
	 * @param pingjia_View
	 * @return
	 */
	@RequestMapping("pingjia_ViewByproduct_idCount")
	@ResponseBody
	public Object pingjia_ViewByproduct_idCount(Pingjia_View pingjia_View) {
		return pingjia_ViewService.pingjia_ViewByproduct_idCount(pingjia_View);
		
	}
}
