package com.hw.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.ProductType;
import com.hw.service.ProductTypeService;
import com.hw.util.Result;

@Controller
public class ProductTypeController {

	@Autowired
	private ProductTypeService ptService;
	
	@RequestMapping("productTypeAll")
	@ResponseBody
	public Object productTypeAll(ProductType pt) {
		pt.setPage((pt.getPage()-1)*pt.getLimit());
		List<ProductType> list = ptService.productTypeAll(pt);
		int count = ptService.productTypeCount(pt);
		if (list.size()>0) {
			return Result.toClient("0", "", count, list);
		} else {
			return Result.toClient("1", "��ѯʧ��");
		}
	}
	
	@RequestMapping("productTypeType")
	@ResponseBody
	public Object productTypeType() {
		return ptService.productTypeType();
		
	}
	
	@RequestMapping("productTypeAdd")
	@ResponseBody
	public Object productTypeAdd(ProductType pt) {
		int add = ptService.productTypeaAdd(pt);
		if (add>0) {
			return Result.toClient("0", "���ӳɹ�");
		} else {
			return Result.toClient("1", "����ʧ��");
		}
	}
	
	@RequestMapping("productTypeUpdate")
	@ResponseBody
	public Object productTypeUpdate(ProductType pt) {
		int update = ptService.productTypeUpdate(pt);
		if (update>0) {
			return Result.toClient("0", "�޸ĳɹ�");
		} else {
			return Result.toClient("1", "�޸�ʧ��");
		}
	}
	
	@RequestMapping("productTypeDelete")
	@ResponseBody
	public Object productTypeDelete(ProductType pt) {
		int delete = ptService.productTypeDelete(pt);
		if (delete>0) {
			return Result.toClient("0", "ɾ���ɹ�");
		} else {
			return Result.toClient("1", "ɾ��ʧ��");
		}
	}
	
	/*
	 * @RequestMapping("productTypeDeletes")
	 * 
	 * @ResponseBody public Object productTypeDeletes(List<Integer> ids) {
	 * 
	 * int deletes = ptService.productTypeDeletes(ids); if (deletes>0) { return
	 * Result.toClient("0", "����ɾ���ɹ�"); } else { return Result.toClient("1",
	 * "����ɾ��ʧ��"); } }
	 */
	
}
