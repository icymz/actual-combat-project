package com.hw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.TuiJian;
import com.hw.service.TuiJianService;
import com.hw.util.Result;

@Controller
public class TuiJianController {
	
	@Autowired
	private TuiJianService tService;
	
	@RequestMapping("tuiJianAdd")
	@ResponseBody
	public Object tuiJianAdd(TuiJian tj) {
		int i = tService.tuiJianAdd(tj);
		if (i>0) {
			return Result.toClient("0", "���ӳɹ�");
		} else {
			return Result.toClient("1", "����ʧ��");
		}
	}
	
	@RequestMapping("tuiJianUpdate")
	@ResponseBody
	public Object tuiJianUpdate(TuiJian tj) {
		int i = tService.tuiJianUpdate(tj);
		if (i>0) {
			return Result.toClient("0", "�޸ĳɹ�");
		} else {
			return Result.toClient("1", "�޸�ʧ��");
		}
	}
	
	@RequestMapping("tuiJianDelete")
	@ResponseBody
	public Object tuiJianDelete(TuiJian tj) {
		int i = tService.tuiJianDelete(tj);
		if (i>0) {
			return Result.toClient("0", "ɾ���ɹ�");
		} else {
			return Result.toClient("1", "ɾ��ʧ��");
		}
	}
}
