package com.hw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Order;
import com.hw.service.OrderService;
import com.hw.util.Result;

@Controller
public class OrderController {

	@Autowired
	private OrderService oService;
	/**
	 * ����
	 * @param o
	 * @return
	 */
	@RequestMapping("orderAdd")
	@ResponseBody
	public Object orderAdd(Order o){
		int add = oService.orderAdd(o);
		if (add>0) {
			return Result.toClient("0", "���ӳɹ�");
		} else {
			return Result.toClient("1", "����ʧ��");
		}
		
	}
	/**
	 * ����
	 * @param o
	 * @return
	 */
	@RequestMapping("orderAdds")
	@ResponseBody
	public Object orderAdds(Order o){
		int add = oService.orderAdds(o);
		if (add>0) {
			return Result.toClient("0", "���ӳɹ�");
		} else {
			return Result.toClient("1", "����ʧ��");
		}
		
	}
	/**
	 * �޸�
	 * @param o
	 * @return
	 */
	@RequestMapping("orderUpdate")
	@ResponseBody
	public Object orderUpdate(Order o){
		int update = oService.orderUpdate(o);
		if (update>0) {
			return Result.toClient("0", "�޸ĳɹ�");
		} else {
			return Result.toClient("1", "�޸�ʧ��");
		}
		
	}
	/**
	 * �޸�
	 * @param o
	 * @return
	 */
	@RequestMapping("orderUpdates")
	@ResponseBody
	public Object orderUpdates(Order o){
		int updates = oService.orderUpdates(o);
		if (updates>0) {
			return Result.toClient("0", "�޸ĳɹ�");
		} else {
			return Result.toClient("1", "�޸�ʧ��");
		}
		
	}
	
	/**
	 * �޸Ĵ�**״̬
	 */
	@RequestMapping("orderUpdateBy1")
	@ResponseBody
	public Object orderUpdateBy1(Order o){
		int updates = oService.orderUpdateBy1(o);
		if (updates>0) {
			return Result.toClient("0", "�޸ĳɹ�");
		} else {
			return Result.toClient("1", "�޸�ʧ��");
		}
		
	}
	
	
	/**
	 * ɾ��
	 * @param order_id
	 * @return
	 */
	@RequestMapping("orderDelete")
	@ResponseBody
	public Object orderDelete(int order_id){
		int delete = oService.orderDelete(order_id);
		if (delete>0) {
			return Result.toClient("0", "ɾ���ɹ�");
		} else {
			return Result.toClient("1", "ɾ��ʧ��");
		}
		
	}
}
