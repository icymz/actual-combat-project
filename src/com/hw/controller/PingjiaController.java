package com.hw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Pingjia;
import com.hw.service.PingjiaService;
import com.hw.util.Result;

@Controller
public class PingjiaController {
	
	@Autowired
	private PingjiaService pingjiaService;
	/**
	 * 添加
	 * @param p
	 * @return
	 */
	@RequestMapping("pingjiaAdd")
	@ResponseBody
	public Object pingjiaAdd(Pingjia p) {
		int add = pingjiaService.pingjiaAdd(p);
		if (add>0) {
			return Result.toClient("0", "添加成功");
		} else {
			return Result.toClient("1", "添加失败");
		}
	}
}
