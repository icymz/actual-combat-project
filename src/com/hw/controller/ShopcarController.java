package com.hw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hw.entity.Shopcar;
import com.hw.service.ShopcarService;

@Controller
public class ShopcarController {
	@Autowired
	private ShopcarService sService;
	
	@RequestMapping("shopAdd")
	@ResponseBody
	public Object shopAdd(Shopcar s) {
		return sService.ShopAdd(s);
	}
	
	@RequestMapping("shopUpdate")
	@ResponseBody
	public Object shopUpdate(Shopcar s) {
		return sService.ShopUpdate(s);
	}
	
	@RequestMapping("shopDelete")
	@ResponseBody
	public Object shopDelete(Shopcar s) {
		return sService.ShopDelete(s);
	}
	
	@RequestMapping("shopDeletes")
	@ResponseBody
	public Object shopDeletes(Shopcar s) {
		return sService.ShopDeletes(s);
	}
}
