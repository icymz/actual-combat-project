package com.hw.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.hw.entity.AdminUser;



public class LoginFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req=(HttpServletRequest)request;
		HttpServletResponse res=(HttpServletResponse)response;
		String url=req.getRequestURI();
		
		HttpSession session=req.getSession();
		AdminUser login=(AdminUser)session.getAttribute("loginUser");
		if(url.endsWith("adminlogin.html") || url.endsWith(".css") || url.endsWith(".js") || url.endsWith(".jpg") ) {
			chain.doFilter(req, res);
		}else if(login!=null) {
			chain.doFilter(req, res);
		}else {
			res.sendRedirect("adminlogin.html");
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
