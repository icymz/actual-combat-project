package com.hw.entity;

public class Shopcar {
	private Integer car_id;
	private Integer car_number;
	private Integer car_product_id;
	private Integer car_user_id;
	private String car_spare1;
	private Integer car_spare2;
	
	
	private Integer page;
	private Integer limit;
	public Shopcar() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Shopcar(Integer car_id, Integer car_number, Integer car_product_id, Integer car_user_id, String car_spare1,
			Integer car_spare2, Integer page, Integer limit) {
		super();
		this.car_id = car_id;
		this.car_number = car_number;
		this.car_product_id = car_product_id;
		this.car_user_id = car_user_id;
		this.car_spare1 = car_spare1;
		this.car_spare2 = car_spare2;
		this.page = page;
		this.limit = limit;
	}
	public Integer getCar_id() {
		return car_id;
	}
	public void setCar_id(Integer car_id) {
		this.car_id = car_id;
	}
	public Integer getCar_number() {
		return car_number;
	}
	public void setCar_number(Integer car_number) {
		this.car_number = car_number;
	}
	public Integer getCar_product_id() {
		return car_product_id;
	}
	public void setCar_product_id(Integer car_product_id) {
		this.car_product_id = car_product_id;
	}
	public Integer getCar_user_id() {
		return car_user_id;
	}
	public void setCar_user_id(Integer car_user_id) {
		this.car_user_id = car_user_id;
	}
	public String getCar_spare1() {
		return car_spare1;
	}
	public void setCar_spare1(String car_spare1) {
		this.car_spare1 = car_spare1;
	}
	public Integer getCar_spare2() {
		return car_spare2;
	}
	public void setCar_spare2(Integer car_spare2) {
		this.car_spare2 = car_spare2;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "Shopcar [car_id=" + car_id + ", car_number=" + car_number + ", car_product_id=" + car_product_id
				+ ", car_user_id=" + car_user_id + ", car_spare1=" + car_spare1 + ", car_spare2=" + car_spare2
				+ ", page=" + page + ", limit=" + limit + "]";
	}
	
}
