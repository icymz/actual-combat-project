package com.hw.entity;

public class TuiJian_view {
	private Integer tj_id;
	private Integer tj_cmdid;
	private Integer tj_weight;
	private String tj_address;
	private String tj_content;
	private Integer tj_spare1;
	private Integer tj_spare2;
	
	private Integer product_id;
	private String product_name;
	private Double product_price;
	private Double product_discountprice;
	private String product_deliveryTime;
	private Integer product_id_type;
	private Integer product_category;
	private Integer product_style;
	private String product_address;
	private Integer product_count;
	private String product_img;
	private String product_launch;
	private String product_isPub;
	private String product_canUse;
	private String product_content;
	private String erro1;
	private String erro2;
	
	
	private Integer begin_weight; 
	private Integer end_weight; 
	private Integer page;
	private Integer limit;
	public TuiJian_view() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TuiJian_view(Integer tj_id, Integer tj_cmdid, Integer tj_weight, String tj_address, String tj_content,
			Integer tj_spare1, Integer tj_spare2, Integer product_id, String product_name, Double product_price,
			Double product_discountprice, String product_deliveryTime, Integer product_id_type,
			Integer product_category, Integer product_style, String product_address, Integer product_count,
			String product_img, String product_launch, String product_isPub, String product_canUse,
			String product_content, String erro1, String erro2, Integer begin_weight, Integer end_weight, Integer page,
			Integer limit) {
		super();
		this.tj_id = tj_id;
		this.tj_cmdid = tj_cmdid;
		this.tj_weight = tj_weight;
		this.tj_address = tj_address;
		this.tj_content = tj_content;
		this.tj_spare1 = tj_spare1;
		this.tj_spare2 = tj_spare2;
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_discountprice = product_discountprice;
		this.product_deliveryTime = product_deliveryTime;
		this.product_id_type = product_id_type;
		this.product_category = product_category;
		this.product_style = product_style;
		this.product_address = product_address;
		this.product_count = product_count;
		this.product_img = product_img;
		this.product_launch = product_launch;
		this.product_isPub = product_isPub;
		this.product_canUse = product_canUse;
		this.product_content = product_content;
		this.erro1 = erro1;
		this.erro2 = erro2;
		this.begin_weight = begin_weight;
		this.end_weight = end_weight;
		this.page = page;
		this.limit = limit;
	}
	public Integer getTj_id() {
		return tj_id;
	}
	public void setTj_id(Integer tj_id) {
		this.tj_id = tj_id;
	}
	public Integer getTj_cmdid() {
		return tj_cmdid;
	}
	public void setTj_cmdid(Integer tj_cmdid) {
		this.tj_cmdid = tj_cmdid;
	}
	public Integer getTj_weight() {
		return tj_weight;
	}
	public void setTj_weight(Integer tj_weight) {
		this.tj_weight = tj_weight;
	}
	public String getTj_address() {
		return tj_address;
	}
	public void setTj_address(String tj_address) {
		this.tj_address = tj_address;
	}
	public String getTj_content() {
		return tj_content;
	}
	public void setTj_content(String tj_content) {
		this.tj_content = tj_content;
	}
	public Integer getTj_spare1() {
		return tj_spare1;
	}
	public void setTj_spare1(Integer tj_spare1) {
		this.tj_spare1 = tj_spare1;
	}
	public Integer getTj_spare2() {
		return tj_spare2;
	}
	public void setTj_spare2(Integer tj_spare2) {
		this.tj_spare2 = tj_spare2;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public Double getProduct_price() {
		return product_price;
	}
	public void setProduct_price(Double product_price) {
		this.product_price = product_price;
	}
	public Double getProduct_discountprice() {
		return product_discountprice;
	}
	public void setProduct_discountprice(Double product_discountprice) {
		this.product_discountprice = product_discountprice;
	}
	public String getProduct_deliveryTime() {
		return product_deliveryTime;
	}
	public void setProduct_deliveryTime(String product_deliveryTime) {
		this.product_deliveryTime = product_deliveryTime;
	}
	public Integer getProduct_id_type() {
		return product_id_type;
	}
	public void setProduct_id_type(Integer product_id_type) {
		this.product_id_type = product_id_type;
	}
	public Integer getProduct_category() {
		return product_category;
	}
	public void setProduct_category(Integer product_category) {
		this.product_category = product_category;
	}
	public Integer getProduct_style() {
		return product_style;
	}
	public void setProduct_style(Integer product_style) {
		this.product_style = product_style;
	}
	public String getProduct_address() {
		return product_address;
	}
	public void setProduct_address(String product_address) {
		this.product_address = product_address;
	}
	public Integer getProduct_count() {
		return product_count;
	}
	public void setProduct_count(Integer product_count) {
		this.product_count = product_count;
	}
	public String getProduct_img() {
		return product_img;
	}
	public void setProduct_img(String product_img) {
		this.product_img = product_img;
	}
	public String getProduct_launch() {
		return product_launch;
	}
	public void setProduct_launch(String product_launch) {
		this.product_launch = product_launch;
	}
	public String getProduct_isPub() {
		return product_isPub;
	}
	public void setProduct_isPub(String product_isPub) {
		this.product_isPub = product_isPub;
	}
	public String getProduct_canUse() {
		return product_canUse;
	}
	public void setProduct_canUse(String product_canUse) {
		this.product_canUse = product_canUse;
	}
	public String getProduct_content() {
		return product_content;
	}
	public void setProduct_content(String product_content) {
		this.product_content = product_content;
	}
	public String getErro1() {
		return erro1;
	}
	public void setErro1(String erro1) {
		this.erro1 = erro1;
	}
	public String getErro2() {
		return erro2;
	}
	public void setErro2(String erro2) {
		this.erro2 = erro2;
	}
	public Integer getBegin_weight() {
		return begin_weight;
	}
	public void setBegin_weight(Integer begin_weight) {
		this.begin_weight = begin_weight;
	}
	public Integer getEnd_weight() {
		return end_weight;
	}
	public void setEnd_weight(Integer end_weight) {
		this.end_weight = end_weight;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "TuiJian_view [tj_id=" + tj_id + ", tj_cmdid=" + tj_cmdid + ", tj_weight=" + tj_weight + ", tj_address="
				+ tj_address + ", tj_content=" + tj_content + ", tj_spare1=" + tj_spare1 + ", tj_spare2=" + tj_spare2
				+ ", product_id=" + product_id + ", product_name=" + product_name + ", product_price=" + product_price
				+ ", product_discountprice=" + product_discountprice + ", product_deliveryTime=" + product_deliveryTime
				+ ", product_id_type=" + product_id_type + ", product_category=" + product_category + ", product_style="
				+ product_style + ", product_address=" + product_address + ", product_count=" + product_count
				+ ", product_img=" + product_img + ", product_launch=" + product_launch + ", product_isPub="
				+ product_isPub + ", product_canUse=" + product_canUse + ", product_content=" + product_content
				+ ", erro1=" + erro1 + ", erro2=" + erro2 + ", begin_weight=" + begin_weight + ", end_weight="
				+ end_weight + ", page=" + page + ", limit=" + limit + "]";
	}
	
	
	
}
