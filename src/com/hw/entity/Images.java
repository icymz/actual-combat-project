package com.hw.entity;

public class Images {
	private Integer img_id;
	private String img_name;
	private String img_address;
	private String spare1;
	private Integer spare2;
	public Integer getImg_id() {
		return img_id;
	}
	public void setImg_id(Integer img_id) {
		this.img_id = img_id;
	}
	public String getImg_name() {
		return img_name;
	}
	public void setImg_name(String img_name) {
		this.img_name = img_name;
	}
	public String getImg_address() {
		return img_address;
	}
	public void setImg_address(String img_address) {
		this.img_address = img_address;
	}
	public String getSpare1() {
		return spare1;
	}
	public void setSpare1(String spare1) {
		this.spare1 = spare1;
	}
	public Integer getSpare2() {
		return spare2;
	}
	public void setSpare2(Integer spare2) {
		this.spare2 = spare2;
	}
	public Images(Integer img_id, String img_name, String img_address, String spare1, Integer spare2) {
		super();
		this.img_id = img_id;
		this.img_name = img_name;
		this.img_address = img_address;
		this.spare1 = spare1;
		this.spare2 = spare2;
	}
	public Images() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Images [img_id=" + img_id + ", img_name=" + img_name + ", img_address=" + img_address + ", spare1="
				+ spare1 + ", spare2=" + spare2 + "]";
	}
	
}
