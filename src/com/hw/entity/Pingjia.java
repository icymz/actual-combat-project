package com.hw.entity;

public class Pingjia {
	private Integer pingjia_id;
	private String  pingjia_content;
	private String pingjia_time;
	private Double pingjia_pingfen;
	private String pingjia_state;
	private Integer pingjia_user_id;
	private Integer pingjia_product_id;
	private String pingjia_img;
	private Integer pingjia_order_id;
	private String pingjia1;
	private Integer pingjia2;
	public Pingjia() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pingjia(Integer pingjia_id, String pingjia_content, String pingjia_time, Double pingjia_pingfen,
			String pingjia_state, Integer pingjia_user_id, Integer pingjia_product_id, String pingjia_img,
			Integer pingjia_order_id, String pingjia1, Integer pingjia2) {
		super();
		this.pingjia_id = pingjia_id;
		this.pingjia_content = pingjia_content;
		this.pingjia_time = pingjia_time;
		this.pingjia_pingfen = pingjia_pingfen;
		this.pingjia_state = pingjia_state;
		this.pingjia_user_id = pingjia_user_id;
		this.pingjia_product_id = pingjia_product_id;
		this.pingjia_img = pingjia_img;
		this.pingjia_order_id = pingjia_order_id;
		this.pingjia1 = pingjia1;
		this.pingjia2 = pingjia2;
	}
	public Integer getPingjia_id() {
		return pingjia_id;
	}
	public void setPingjia_id(Integer pingjia_id) {
		this.pingjia_id = pingjia_id;
	}
	public String getPingjia_content() {
		return pingjia_content;
	}
	public void setPingjia_content(String pingjia_content) {
		this.pingjia_content = pingjia_content;
	}
	public String getPingjia_time() {
		return pingjia_time;
	}
	public void setPingjia_time(String pingjia_time) {
		this.pingjia_time = pingjia_time;
	}
	public Double getPingjia_pingfen() {
		return pingjia_pingfen;
	}
	public void setPingjia_pingfen(Double pingjia_pingfen) {
		this.pingjia_pingfen = pingjia_pingfen;
	}
	public String getPingjia_state() {
		return pingjia_state;
	}
	public void setPingjia_state(String pingjia_state) {
		this.pingjia_state = pingjia_state;
	}
	public Integer getPingjia_user_id() {
		return pingjia_user_id;
	}
	public void setPingjia_user_id(Integer pingjia_user_id) {
		this.pingjia_user_id = pingjia_user_id;
	}
	public Integer getPingjia_product_id() {
		return pingjia_product_id;
	}
	public void setPingjia_product_id(Integer pingjia_product_id) {
		this.pingjia_product_id = pingjia_product_id;
	}
	public String getPingjia_img() {
		return pingjia_img;
	}
	public void setPingjia_img(String pingjia_img) {
		this.pingjia_img = pingjia_img;
	}
	public Integer getPingjia_order_id() {
		return pingjia_order_id;
	}
	public void setPingjia_order_id(Integer pingjia_order_id) {
		this.pingjia_order_id = pingjia_order_id;
	}
	public String getPingjia1() {
		return pingjia1;
	}
	public void setPingjia1(String pingjia1) {
		this.pingjia1 = pingjia1;
	}
	public Integer getPingjia2() {
		return pingjia2;
	}
	public void setPingjia2(Integer pingjia2) {
		this.pingjia2 = pingjia2;
	}
	@Override
	public String toString() {
		return "Pingjia [pingjia_id=" + pingjia_id + ", pingjia_content=" + pingjia_content + ", pingjia_time="
				+ pingjia_time + ", pingjia_pingfen=" + pingjia_pingfen + ", pingjia_state=" + pingjia_state
				+ ", pingjia_user_id=" + pingjia_user_id + ", pingjia_product_id=" + pingjia_product_id
				+ ", pingjia_img=" + pingjia_img + ", pingjia_order_id=" + pingjia_order_id + ", pingjia1=" + pingjia1
				+ ", pingjia2=" + pingjia2 + "]";
	}
	
	
}
