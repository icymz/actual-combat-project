package com.hw.entity;

public class AdminUser {
	private Integer id;
	private String userName;
	private String password;
	private String ext1;
	private Integer ext2;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getExt1() {
		return ext1;
	}
	public void setExt1(String ext1) {
		this.ext1 = ext1;
	}
	public Integer getExt2() {
		return ext2;
	}
	public void setExt2(Integer ext2) {
		this.ext2 = ext2;
	}
	public AdminUser(Integer id, String userName, String password, String ext1, Integer ext2) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.ext1 = ext1;
		this.ext2 = ext2;
	}
	public AdminUser() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "adminuser [id=" + id + ", userName=" + userName + ", password=" + password + ", ext1=" + ext1
				+ ", ext2=" + ext2 + "]";
	}
	
}
