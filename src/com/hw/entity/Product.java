package com.hw.entity;

public class Product {
	private Integer product_id;
	private String product_name;
	private Double product_price;
	private Double product_discountprice;
	private String product_deliveryTime;
	private Integer product_id_type;
	private Integer product_category;
	private Integer product_style;
	private String product_address;
	private Integer product_count;
	private String product_img;
	private String product_launch;
	private String product_isPub;
	private String product_canUse;
	private String product_content;
	private String erro1;
	private String erro2;
	
	private ProductType productType;
	
	private Double begin_discountprice;
	private Double end_discountprice;
	private int page;
	private int limit;
	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Product(Integer product_id, String product_name, Double product_price, Double product_discountprice,
			String product_deliveryTime, Integer product_id_type, Integer product_category, Integer product_style,
			String product_address, Integer product_count, String product_img, String product_launch,
			String product_isPub, String product_canUse, String product_content, String erro1, String erro2,
			ProductType productType, Double begin_discountprice, Double end_discountprice, int page, int limit) {
		super();
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_discountprice = product_discountprice;
		this.product_deliveryTime = product_deliveryTime;
		this.product_id_type = product_id_type;
		this.product_category = product_category;
		this.product_style = product_style;
		this.product_address = product_address;
		this.product_count = product_count;
		this.product_img = product_img;
		this.product_launch = product_launch;
		this.product_isPub = product_isPub;
		this.product_canUse = product_canUse;
		this.product_content = product_content;
		this.erro1 = erro1;
		this.erro2 = erro2;
		this.productType = productType;
		this.begin_discountprice = begin_discountprice;
		this.end_discountprice = end_discountprice;
		this.page = page;
		this.limit = limit;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public Double getProduct_price() {
		return product_price;
	}
	public void setProduct_price(Double product_price) {
		this.product_price = product_price;
	}
	public Double getProduct_discountprice() {
		return product_discountprice;
	}
	public void setProduct_discountprice(Double product_discountprice) {
		this.product_discountprice = product_discountprice;
	}
	public String getProduct_deliveryTime() {
		return product_deliveryTime;
	}
	public void setProduct_deliveryTime(String product_deliveryTime) {
		this.product_deliveryTime = product_deliveryTime;
	}
	public Integer getProduct_id_type() {
		return product_id_type;
	}
	public void setProduct_id_type(Integer product_id_type) {
		this.product_id_type = product_id_type;
	}
	public Integer getProduct_category() {
		return product_category;
	}
	public void setProduct_category(Integer product_category) {
		this.product_category = product_category;
	}
	public Integer getProduct_style() {
		return product_style;
	}
	public void setProduct_style(Integer product_style) {
		this.product_style = product_style;
	}
	public String getProduct_address() {
		return product_address;
	}
	public void setProduct_address(String product_address) {
		this.product_address = product_address;
	}
	public Integer getProduct_count() {
		return product_count;
	}
	public void setProduct_count(Integer product_count) {
		this.product_count = product_count;
	}
	public String getProduct_img() {
		return product_img;
	}
	public void setProduct_img(String product_img) {
		this.product_img = product_img;
	}
	public String getProduct_launch() {
		return product_launch;
	}
	public void setProduct_launch(String product_launch) {
		this.product_launch = product_launch;
	}
	public String getProduct_isPub() {
		return product_isPub;
	}
	public void setProduct_isPub(String product_isPub) {
		this.product_isPub = product_isPub;
	}
	public String getProduct_canUse() {
		return product_canUse;
	}
	public void setProduct_canUse(String product_canUse) {
		this.product_canUse = product_canUse;
	}
	public String getProduct_content() {
		return product_content;
	}
	public void setProduct_content(String product_content) {
		this.product_content = product_content;
	}
	public String getErro1() {
		return erro1;
	}
	public void setErro1(String erro1) {
		this.erro1 = erro1;
	}
	public String getErro2() {
		return erro2;
	}
	public void setErro2(String erro2) {
		this.erro2 = erro2;
	}
	public ProductType getProductType() {
		return productType;
	}
	public void setProductType(ProductType productType) {
		this.productType = productType;
	}
	public Double getBegin_discountprice() {
		return begin_discountprice;
	}
	public void setBegin_discountprice(Double begin_discountprice) {
		this.begin_discountprice = begin_discountprice;
	}
	public Double getEnd_discountprice() {
		return end_discountprice;
	}
	public void setEnd_discountprice(Double end_discountprice) {
		this.end_discountprice = end_discountprice;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "Product [product_id=" + product_id + ", product_name=" + product_name + ", product_price="
				+ product_price + ", product_discountprice=" + product_discountprice + ", product_deliveryTime="
				+ product_deliveryTime + ", product_id_type=" + product_id_type + ", product_category="
				+ product_category + ", product_style=" + product_style + ", product_address=" + product_address
				+ ", product_count=" + product_count + ", product_img=" + product_img + ", product_launch="
				+ product_launch + ", product_isPub=" + product_isPub + ", product_canUse=" + product_canUse
				+ ", product_content=" + product_content + ", erro1=" + erro1 + ", erro2=" + erro2 + ", productType="
				+ productType + ", begin_discountprice=" + begin_discountprice + ", end_discountprice="
				+ end_discountprice + ", page=" + page + ", limit=" + limit + "]";
	}
	
	
}
