package com.hw.entity;

public class TuiJian {
	private Integer tj_id;
	private Integer tj_cmdid;
	private Integer tj_weight;
	private String tj_address;
	private String tj_content;
	
	private Integer tj_spare1;
	private Integer tj_spare2;
	public TuiJian() {
		super();
		
	}
	public TuiJian(Integer tj_id, Integer tj_cmdid, Integer tj_weight, String tj_address, String tj_content,
			Integer tj_spare1, Integer tj_spare2) {
		super();
		this.tj_id = tj_id;
		this.tj_cmdid = tj_cmdid;
		this.tj_weight = tj_weight;
		this.tj_address = tj_address;
		this.tj_content = tj_content;
		this.tj_spare1 = tj_spare1;
		this.tj_spare2 = tj_spare2;
	}
	public Integer getTj_id() {
		return tj_id;
	}
	public void setTj_id(Integer tj_id) {
		this.tj_id = tj_id;
	}
	public Integer getTj_cmdid() {
		return tj_cmdid;
	}
	public void setTj_cmdid(Integer tj_cmdid) {
		this.tj_cmdid = tj_cmdid;
	}
	public Integer getTj_weight() {
		return tj_weight;
	}
	public void setTj_weight(Integer tj_weight) {
		this.tj_weight = tj_weight;
	}
	public String getTj_address() {
		return tj_address;
	}
	public void setTj_address(String tj_address) {
		this.tj_address = tj_address;
	}
	public String getTj_content() {
		return tj_content;
	}
	public void setTj_content(String tj_content) {
		this.tj_content = tj_content;
	}
	public Integer getTj_spare1() {
		return tj_spare1;
	}
	public void setTj_spare1(Integer tj_spare1) {
		this.tj_spare1 = tj_spare1;
	}
	public Integer getTj_spare2() {
		return tj_spare2;
	}
	public void setTj_spare2(Integer tj_spare2) {
		this.tj_spare2 = tj_spare2;
	}
	@Override
	public String toString() {
		return "TuiJian [tj_id=" + tj_id + ", tj_cmdid=" + tj_cmdid + ", tj_weight=" + tj_weight + ", tj_address="
				+ tj_address + ", tj_content=" + tj_content + ", tj_spare1=" + tj_spare1 + ", tj_spare2=" + tj_spare2
				+ "]";
	}

	
}
