package com.hw.entity;

public class Address {
	private Integer address_id;
	private String address_country;
	private String address_province;
	private String address_city;
	private String address_town;
	private String address_addss;
	private String address_pc;
	private Integer address_userid;
	private String address_phone;
	private String address_username;
	
	private int page;
	private int limit;
	public Address() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Address(Integer address_id, String address_country, String address_province, String address_city,
			String address_town, String address_addss, String address_pc, Integer address_userid, String address_phone,
			String address_username, int page, int limit) {
		super();
		this.address_id = address_id;
		this.address_country = address_country;
		this.address_province = address_province;
		this.address_city = address_city;
		this.address_town = address_town;
		this.address_addss = address_addss;
		this.address_pc = address_pc;
		this.address_userid = address_userid;
		this.address_phone = address_phone;
		this.address_username = address_username;
		this.page = page;
		this.limit = limit;
	}
	public Integer getAddress_id() {
		return address_id;
	}
	public void setAddress_id(Integer address_id) {
		this.address_id = address_id;
	}
	public String getaddress_country() {
		return address_country;
	}
	public void setaddress_country(String address_country) {
		this.address_country = address_country;
	}
	public String getAddress_province() {
		return address_province;
	}
	public void setAddress_province(String address_province) {
		this.address_province = address_province;
	}
	public String getAddress_city() {
		return address_city;
	}
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}
	public String getAddress_town() {
		return address_town;
	}
	public void setAddress_town(String address_town) {
		this.address_town = address_town;
	}
	public String getAddress_addss() {
		return address_addss;
	}
	public void setAddress_addss(String address_addss) {
		this.address_addss = address_addss;
	}
	public String getAddress_pc() {
		return address_pc;
	}
	public void setAddress_pc(String address_pc) {
		this.address_pc = address_pc;
	}
	public Integer getAddress_userid() {
		return address_userid;
	}
	public void setAddress_userid(Integer address_userid) {
		this.address_userid = address_userid;
	}
	public String getAddress_phone() {
		return address_phone;
	}
	public void setAddress_phone(String address_phone) {
		this.address_phone = address_phone;
	}
	public String getAddress_username() {
		return address_username;
	}
	public void setAddress_username(String address_username) {
		this.address_username = address_username;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "Address [address_id=" + address_id + ", address_country=" + address_country + ", address_province="
				+ address_province + ", address_city=" + address_city + ", address_town=" + address_town
				+ ", address_addss=" + address_addss + ", address_pc=" + address_pc + ", address_userid="
				+ address_userid + ", address_phone=" + address_phone + ", address_username=" + address_username
				+ ", page=" + page + ", limit=" + limit + "]";
	}
	
}
