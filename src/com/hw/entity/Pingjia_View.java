package com.hw.entity;

public class Pingjia_View {
	private Integer pingjia_id;
	private String  pingjia_content;
	private String pingjia_time;
	private Double pingjia_pingfen;
	private String pingjia_state;
	private Integer pingjia_user_id;
	private Integer pingjia_product_id;
	private String pingjia_img;
	private Integer pingjia_order_id;
	private String pingjia1;
	private Integer pingjia2;
	
	
	private Integer user_id;
	private String user_loginname;
	private String user_pwd;
	private String user_fullname;
	private String user_sex;
	private String user_birthday;
	private String user_address;
	private String user_phone;
	private String user_email;
	private String user_img;
	
	
	private Integer product_id;
	private String product_name;
	private Double product_price;
	private Double product_discountprice;
	private String product_deliveryTime;
	private Integer product_id_type;
	private Integer product_category;
	private Integer product_style;
	private String product_address;
	private Integer product_count;
	private String product_img;
	private String product_launch;
	private String product_isPub;
	private String product_canUse;
	private String product_content;
	private String erro1;
	private String erro2;
	
	
	private Integer order_id;
	private String order_name;
	private String order_time;
	private String order_ship;
	private String order_money;
	private Integer order_product_id;
	private Integer order_user_id;
	private Integer order_address_id;
	private String order_on;
	private String order_state;
	private String order_spare1;
	private String order_spare2;
	
	private Integer page;
	private Integer limit;
	public Pingjia_View() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pingjia_View(Integer pingjia_id, String pingjia_content, String pingjia_time, Double pingjia_pingfen,
			String pingjia_state, Integer pingjia_user_id, Integer pingjia_product_id, String pingjia_img,
			Integer pingjia_order_id, String pingjia1, Integer pingjia2, Integer user_id, String user_loginname,
			String user_pwd, String user_fullname, String user_sex, String user_birthday, String user_address,
			String user_phone, String user_email, String user_img, Integer product_id, String product_name,
			Double product_price, Double product_discountprice, String product_deliveryTime, Integer product_id_type,
			Integer product_category, Integer product_style, String product_address, Integer product_count,
			String product_img, String product_launch, String product_isPub, String product_canUse,
			String product_content, String erro1, String erro2, Integer order_id, String order_name, String order_time,
			String order_ship, String order_money, Integer order_product_id, Integer order_user_id,
			Integer order_address_id, String order_on, String order_state, String order_spare1, String order_spare2,
			Integer page, Integer limit) {
		super();
		this.pingjia_id = pingjia_id;
		this.pingjia_content = pingjia_content;
		this.pingjia_time = pingjia_time;
		this.pingjia_pingfen = pingjia_pingfen;
		this.pingjia_state = pingjia_state;
		this.pingjia_user_id = pingjia_user_id;
		this.pingjia_product_id = pingjia_product_id;
		this.pingjia_img = pingjia_img;
		this.pingjia_order_id = pingjia_order_id;
		this.pingjia1 = pingjia1;
		this.pingjia2 = pingjia2;
		this.user_id = user_id;
		this.user_loginname = user_loginname;
		this.user_pwd = user_pwd;
		this.user_fullname = user_fullname;
		this.user_sex = user_sex;
		this.user_birthday = user_birthday;
		this.user_address = user_address;
		this.user_phone = user_phone;
		this.user_email = user_email;
		this.user_img = user_img;
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_discountprice = product_discountprice;
		this.product_deliveryTime = product_deliveryTime;
		this.product_id_type = product_id_type;
		this.product_category = product_category;
		this.product_style = product_style;
		this.product_address = product_address;
		this.product_count = product_count;
		this.product_img = product_img;
		this.product_launch = product_launch;
		this.product_isPub = product_isPub;
		this.product_canUse = product_canUse;
		this.product_content = product_content;
		this.erro1 = erro1;
		this.erro2 = erro2;
		this.order_id = order_id;
		this.order_name = order_name;
		this.order_time = order_time;
		this.order_ship = order_ship;
		this.order_money = order_money;
		this.order_product_id = order_product_id;
		this.order_user_id = order_user_id;
		this.order_address_id = order_address_id;
		this.order_on = order_on;
		this.order_state = order_state;
		this.order_spare1 = order_spare1;
		this.order_spare2 = order_spare2;
		this.page = page;
		this.limit = limit;
	}
	public Integer getPingjia_id() {
		return pingjia_id;
	}
	public void setPingjia_id(Integer pingjia_id) {
		this.pingjia_id = pingjia_id;
	}
	public String getPingjia_content() {
		return pingjia_content;
	}
	public void setPingjia_content(String pingjia_content) {
		this.pingjia_content = pingjia_content;
	}
	public String getPingjia_time() {
		return pingjia_time;
	}
	public void setPingjia_time(String pingjia_time) {
		this.pingjia_time = pingjia_time;
	}
	public Double getPingjia_pingfen() {
		return pingjia_pingfen;
	}
	public void setPingjia_pingfen(Double pingjia_pingfen) {
		this.pingjia_pingfen = pingjia_pingfen;
	}
	public String getPingjia_state() {
		return pingjia_state;
	}
	public void setPingjia_state(String pingjia_state) {
		this.pingjia_state = pingjia_state;
	}
	public Integer getPingjia_user_id() {
		return pingjia_user_id;
	}
	public void setPingjia_user_id(Integer pingjia_user_id) {
		this.pingjia_user_id = pingjia_user_id;
	}
	public Integer getPingjia_product_id() {
		return pingjia_product_id;
	}
	public void setPingjia_product_id(Integer pingjia_product_id) {
		this.pingjia_product_id = pingjia_product_id;
	}
	public String getPingjia_img() {
		return pingjia_img;
	}
	public void setPingjia_img(String pingjia_img) {
		this.pingjia_img = pingjia_img;
	}
	public Integer getPingjia_order_id() {
		return pingjia_order_id;
	}
	public void setPingjia_order_id(Integer pingjia_order_id) {
		this.pingjia_order_id = pingjia_order_id;
	}
	public String getPingjia1() {
		return pingjia1;
	}
	public void setPingjia1(String pingjia1) {
		this.pingjia1 = pingjia1;
	}
	public Integer getPingjia2() {
		return pingjia2;
	}
	public void setPingjia2(Integer pingjia2) {
		this.pingjia2 = pingjia2;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_loginname() {
		return user_loginname;
	}
	public void setUser_loginname(String user_loginname) {
		this.user_loginname = user_loginname;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getUser_fullname() {
		return user_fullname;
	}
	public void setUser_fullname(String user_fullname) {
		this.user_fullname = user_fullname;
	}
	public String getUser_sex() {
		return user_sex;
	}
	public void setUser_sex(String user_sex) {
		this.user_sex = user_sex;
	}
	public String getUser_birthday() {
		return user_birthday;
	}
	public void setUser_birthday(String user_birthday) {
		this.user_birthday = user_birthday;
	}
	public String getUser_address() {
		return user_address;
	}
	public void setUser_address(String user_address) {
		this.user_address = user_address;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_img() {
		return user_img;
	}
	public void setUser_img(String user_img) {
		this.user_img = user_img;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public Double getProduct_price() {
		return product_price;
	}
	public void setProduct_price(Double product_price) {
		this.product_price = product_price;
	}
	public Double getProduct_discountprice() {
		return product_discountprice;
	}
	public void setProduct_discountprice(Double product_discountprice) {
		this.product_discountprice = product_discountprice;
	}
	public String getProduct_deliveryTime() {
		return product_deliveryTime;
	}
	public void setProduct_deliveryTime(String product_deliveryTime) {
		this.product_deliveryTime = product_deliveryTime;
	}
	public Integer getProduct_id_type() {
		return product_id_type;
	}
	public void setProduct_id_type(Integer product_id_type) {
		this.product_id_type = product_id_type;
	}
	public Integer getProduct_category() {
		return product_category;
	}
	public void setProduct_category(Integer product_category) {
		this.product_category = product_category;
	}
	public Integer getProduct_style() {
		return product_style;
	}
	public void setProduct_style(Integer product_style) {
		this.product_style = product_style;
	}
	public String getProduct_address() {
		return product_address;
	}
	public void setProduct_address(String product_address) {
		this.product_address = product_address;
	}
	public Integer getProduct_count() {
		return product_count;
	}
	public void setProduct_count(Integer product_count) {
		this.product_count = product_count;
	}
	public String getProduct_img() {
		return product_img;
	}
	public void setProduct_img(String product_img) {
		this.product_img = product_img;
	}
	public String getProduct_launch() {
		return product_launch;
	}
	public void setProduct_launch(String product_launch) {
		this.product_launch = product_launch;
	}
	public String getProduct_isPub() {
		return product_isPub;
	}
	public void setProduct_isPub(String product_isPub) {
		this.product_isPub = product_isPub;
	}
	public String getProduct_canUse() {
		return product_canUse;
	}
	public void setProduct_canUse(String product_canUse) {
		this.product_canUse = product_canUse;
	}
	public String getProduct_content() {
		return product_content;
	}
	public void setProduct_content(String product_content) {
		this.product_content = product_content;
	}
	public String getErro1() {
		return erro1;
	}
	public void setErro1(String erro1) {
		this.erro1 = erro1;
	}
	public String getErro2() {
		return erro2;
	}
	public void setErro2(String erro2) {
		this.erro2 = erro2;
	}
	public Integer getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}
	public String getOrder_name() {
		return order_name;
	}
	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}
	public String getOrder_time() {
		return order_time;
	}
	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}
	public String getOrder_ship() {
		return order_ship;
	}
	public void setOrder_ship(String order_ship) {
		this.order_ship = order_ship;
	}
	public String getOrder_money() {
		return order_money;
	}
	public void setOrder_money(String order_money) {
		this.order_money = order_money;
	}
	public Integer getOrder_product_id() {
		return order_product_id;
	}
	public void setOrder_product_id(Integer order_product_id) {
		this.order_product_id = order_product_id;
	}
	public Integer getOrder_user_id() {
		return order_user_id;
	}
	public void setOrder_user_id(Integer order_user_id) {
		this.order_user_id = order_user_id;
	}
	public Integer getOrder_address_id() {
		return order_address_id;
	}
	public void setOrder_address_id(Integer order_address_id) {
		this.order_address_id = order_address_id;
	}
	public String getOrder_on() {
		return order_on;
	}
	public void setOrder_on(String order_on) {
		this.order_on = order_on;
	}
	public String getOrder_state() {
		return order_state;
	}
	public void setOrder_state(String order_state) {
		this.order_state = order_state;
	}
	public String getOrder_spare1() {
		return order_spare1;
	}
	public void setOrder_spare1(String order_spare1) {
		this.order_spare1 = order_spare1;
	}
	public String getOrder_spare2() {
		return order_spare2;
	}
	public void setOrder_spare2(String order_spare2) {
		this.order_spare2 = order_spare2;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "Pingjia_View [pingjia_id=" + pingjia_id + ", pingjia_content=" + pingjia_content + ", pingjia_time="
				+ pingjia_time + ", pingjia_pingfen=" + pingjia_pingfen + ", pingjia_state=" + pingjia_state
				+ ", pingjia_user_id=" + pingjia_user_id + ", pingjia_product_id=" + pingjia_product_id
				+ ", pingjia_img=" + pingjia_img + ", pingjia_order_id=" + pingjia_order_id + ", pingjia1=" + pingjia1
				+ ", pingjia2=" + pingjia2 + ", user_id=" + user_id + ", user_loginname=" + user_loginname
				+ ", user_pwd=" + user_pwd + ", user_fullname=" + user_fullname + ", user_sex=" + user_sex
				+ ", user_birthday=" + user_birthday + ", user_address=" + user_address + ", user_phone=" + user_phone
				+ ", user_email=" + user_email + ", user_img=" + user_img + ", product_id=" + product_id
				+ ", product_name=" + product_name + ", product_price=" + product_price + ", product_discountprice="
				+ product_discountprice + ", product_deliveryTime=" + product_deliveryTime + ", product_id_type="
				+ product_id_type + ", product_category=" + product_category + ", product_style=" + product_style
				+ ", product_address=" + product_address + ", product_count=" + product_count + ", product_img="
				+ product_img + ", product_launch=" + product_launch + ", product_isPub=" + product_isPub
				+ ", product_canUse=" + product_canUse + ", product_content=" + product_content + ", erro1=" + erro1
				+ ", erro2=" + erro2 + ", order_id=" + order_id + ", order_name=" + order_name + ", order_time="
				+ order_time + ", order_ship=" + order_ship + ", order_money=" + order_money + ", order_product_id="
				+ order_product_id + ", order_user_id=" + order_user_id + ", order_address_id=" + order_address_id
				+ ", order_on=" + order_on + ", order_state=" + order_state + ", order_spare1=" + order_spare1
				+ ", order_spare2=" + order_spare2 + ", page=" + page + ", limit=" + limit + "]";
	}
	
	
}
