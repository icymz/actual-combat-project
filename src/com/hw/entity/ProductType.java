package com.hw.entity;

public class ProductType {
	private Integer t_id;
	private String t_name;
	private String t_img;
	private String shelby1;
	private Integer shelby2;
	
	private Integer page;
	private Integer limit;
	public ProductType() {
		super();
		// TODO Auto-generated constructor stub
	}
	public ProductType(Integer t_id, String t_name, String t_img, String shelby1, Integer shelby2, Integer page,
			Integer limit) {
		super();
		this.t_id = t_id;
		this.t_name = t_name;
		this.t_img = t_img;
		this.shelby1 = shelby1;
		this.shelby2 = shelby2;
		this.page = page;
		this.limit = limit;
	}
	public Integer getT_id() {
		return t_id;
	}
	public void setT_id(Integer t_id) {
		this.t_id = t_id;
	}
	public String getT_name() {
		return t_name;
	}
	public void setT_name(String t_name) {
		this.t_name = t_name;
	}
	public String getT_img() {
		return t_img;
	}
	public void setT_img(String t_img) {
		this.t_img = t_img;
	}
	public String getShelby1() {
		return shelby1;
	}
	public void setShelby1(String shelby1) {
		this.shelby1 = shelby1;
	}
	public Integer getShelby2() {
		return shelby2;
	}
	public void setShelby2(Integer shelby2) {
		this.shelby2 = shelby2;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "ProductType [t_id=" + t_id + ", t_name=" + t_name + ", t_img=" + t_img + ", shelby1=" + shelby1
				+ ", shelby2=" + shelby2 + ", page=" + page + ", limit=" + limit + "]";
	}
	
	
	
}
