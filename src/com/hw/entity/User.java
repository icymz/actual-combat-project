package com.hw.entity;

public class User {
	private Integer user_id;
	private String user_loginname;
	private String user_pwd;
	private String user_fullname;
	private String user_sex;
	private String user_birthday;
	private String user_address;
	private String user_phone;
	private String user_email;
	private String user_img;
	
	private int page;
	private int limit;
	public User(Integer user_id, String user_loginname, String user_pwd, String user_fullname, String user_sex,
			String user_birthday, String user_address, String user_phone, String user_email, String user_img, int page,
			int limit) {
		super();
		this.user_id = user_id;
		this.user_loginname = user_loginname;
		this.user_pwd = user_pwd;
		this.user_fullname = user_fullname;
		this.user_sex = user_sex;
		this.user_birthday = user_birthday;
		this.user_address = user_address;
		this.user_phone = user_phone;
		this.user_email = user_email;
		this.user_img = user_img;
		this.page = page;
		this.limit = limit;
	}
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_loginname() {
		return user_loginname;
	}
	public void setUser_loginname(String user_loginname) {
		this.user_loginname = user_loginname;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getUser_fullname() {
		return user_fullname;
	}
	public void setUser_fullname(String user_fullname) {
		this.user_fullname = user_fullname;
	}
	public String getUser_sex() {
		return user_sex;
	}
	public void setUser_sex(String user_sex) {
		this.user_sex = user_sex;
	}
	public String getUser_birthday() {
		return user_birthday;
	}
	public void setUser_birthday(String user_birthday) {
		this.user_birthday = user_birthday;
	}
	public String getUser_address() {
		return user_address;
	}
	public void setUser_address(String user_address) {
		this.user_address = user_address;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_img() {
		return user_img;
	}
	public void setUser_img(String user_img) {
		this.user_img = user_img;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "User [user_id=" + user_id + ", user_loginname=" + user_loginname + ", user_pwd=" + user_pwd
				+ ", user_fullname=" + user_fullname + ", user_sex=" + user_sex + ", user_birthday=" + user_birthday
				+ ", user_address=" + user_address + ", user_phone=" + user_phone + ", user_email=" + user_email
				+ ", user_img=" + user_img + ", page=" + page + ", limit=" + limit + "]";
	}

	
}
