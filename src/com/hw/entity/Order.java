package com.hw.entity;

public class Order {
	private Integer order_id;
	private String order_name;
	private String order_time;
	private String order_ship;
	private String order_money;
	private Integer order_product_id;
	private Integer order_user_id;
	private Integer order_address_id;
	private String order_on;
	private String order_state;
	private String order_spare1;
	private String order_spare2;
	public Order() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Order(Integer order_id, String order_name, String order_time, String order_ship, String order_money,
			Integer order_product_id, Integer order_user_id, Integer order_address_id, String order_on,
			String order_state, String order_spare1, String order_spare2) {
		super();
		this.order_id = order_id;
		this.order_name = order_name;
		this.order_time = order_time;
		this.order_ship = order_ship;
		this.order_money = order_money;
		this.order_product_id = order_product_id;
		this.order_user_id = order_user_id;
		this.order_address_id = order_address_id;
		this.order_on = order_on;
		this.order_state = order_state;
		this.order_spare1 = order_spare1;
		this.order_spare2 = order_spare2;
	}
	public Integer getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}
	public String getOrder_name() {
		return order_name;
	}
	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}
	public String getOrder_time() {
		return order_time;
	}
	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}
	public String getOrder_ship() {
		return order_ship;
	}
	public void setOrder_ship(String order_ship) {
		this.order_ship = order_ship;
	}
	public String getOrder_money() {
		return order_money;
	}
	public void setOrder_money(String order_money) {
		this.order_money = order_money;
	}
	public Integer getOrder_product_id() {
		return order_product_id;
	}
	public void setOrder_product_id(Integer order_product_id) {
		this.order_product_id = order_product_id;
	}
	public Integer getOrder_user_id() {
		return order_user_id;
	}
	public void setOrder_user_id(Integer order_user_id) {
		this.order_user_id = order_user_id;
	}
	public Integer getOrder_address_id() {
		return order_address_id;
	}
	public void setOrder_address_id(Integer order_address_id) {
		this.order_address_id = order_address_id;
	}
	public String getOrder_on() {
		return order_on;
	}
	public void setOrder_on(String order_on) {
		this.order_on = order_on;
	}
	public String getOrder_state() {
		return order_state;
	}
	public void setOrder_state(String order_state) {
		this.order_state = order_state;
	}
	public String getOrder_spare1() {
		return order_spare1;
	}
	public void setOrder_spare1(String order_spare1) {
		this.order_spare1 = order_spare1;
	}
	public String getOrder_spare2() {
		return order_spare2;
	}
	public void setOrder_spare2(String order_spare2) {
		this.order_spare2 = order_spare2;
	}
	@Override
	public String toString() {
		return "Order [order_id=" + order_id + ", order_name=" + order_name + ", order_time=" + order_time
				+ ", order_ship=" + order_ship + ", order_money=" + order_money + ", order_product_id="
				+ order_product_id + ", order_user_id=" + order_user_id + ", order_address_id=" + order_address_id
				+ ", order_on=" + order_on + ", order_state=" + order_state + ", order_spare1=" + order_spare1
				+ ", order_spare2=" + order_spare2 + "]";
	}
	
}
