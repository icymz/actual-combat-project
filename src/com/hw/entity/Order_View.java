package com.hw.entity;

public class Order_View {
	private Integer order_id;
	private String order_name;
	private String order_time;
	private String order_ship;
	private String order_money;
	private Integer order_product_id;
	private Integer order_user_id;
	private Integer order_address_id;
	private String order_on;
	private String order_state;
	private String order_spare1;
	private String order_spare2;
	
	private String begin_order_time;
	private String end_order_time;
	
	
	private Integer product_id;
	private String product_name;
	private Double product_price;
	private Double product_discountprice;
	private String product_deliveryTime;
	private Integer product_id_type;
	private Integer product_category;
	private Integer product_style;
	private String product_address;
	private Integer product_count;
	private String product_img;
	private String product_launch;
	private String product_isPub;
	private String product_canUse;
	private String product_content;
	
	
	
	
	private Integer user_id;
    private String user_loginname;
	private String user_pwd;
	private String user_sex;
	private String user_birthday;
	private String user_address;
	private String user_spare1;
	private String user_spare2;
	private Integer user_spare3;
	
	
	private Integer address_id;
	private String adress_country;
	private String address_province;
	private String address_city;
	private String address_town;
	private String address_addss;
	private String address_pc;
	private Integer address_userid;
	private String address_phone;
	private String address_username;
	
	
	private int page;
	private int limit;
	public Order_View() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Order_View(Integer order_id, String order_name, String order_time, String order_ship, String order_money,
			Integer order_product_id, Integer order_user_id, Integer order_address_id, String order_on,
			String order_state, String order_spare1, String order_spare2, String begin_order_time,
			String end_order_time, Integer product_id, String product_name, Double product_price,
			Double product_discountprice, String product_deliveryTime, Integer product_id_type,
			Integer product_category, Integer product_style, String product_address, Integer product_count,
			String product_img, String product_launch, String product_isPub, String product_canUse,
			String product_content, Integer user_id, String user_loginname, String user_pwd, String user_sex,
			String user_birthday, String user_address, String user_spare1, String user_spare2, Integer user_spare3,
			Integer address_id, String adress_country, String address_province, String address_city,
			String address_town, String address_addss, String address_pc, Integer address_userid, String address_phone,
			String address_username, int page, int limit) {
		super();
		this.order_id = order_id;
		this.order_name = order_name;
		this.order_time = order_time;
		this.order_ship = order_ship;
		this.order_money = order_money;
		this.order_product_id = order_product_id;
		this.order_user_id = order_user_id;
		this.order_address_id = order_address_id;
		this.order_on = order_on;
		this.order_state = order_state;
		this.order_spare1 = order_spare1;
		this.order_spare2 = order_spare2;
		this.begin_order_time = begin_order_time;
		this.end_order_time = end_order_time;
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_discountprice = product_discountprice;
		this.product_deliveryTime = product_deliveryTime;
		this.product_id_type = product_id_type;
		this.product_category = product_category;
		this.product_style = product_style;
		this.product_address = product_address;
		this.product_count = product_count;
		this.product_img = product_img;
		this.product_launch = product_launch;
		this.product_isPub = product_isPub;
		this.product_canUse = product_canUse;
		this.product_content = product_content;
		this.user_id = user_id;
		this.user_loginname = user_loginname;
		this.user_pwd = user_pwd;
		this.user_sex = user_sex;
		this.user_birthday = user_birthday;
		this.user_address = user_address;
		this.user_spare1 = user_spare1;
		this.user_spare2 = user_spare2;
		this.user_spare3 = user_spare3;
		this.address_id = address_id;
		this.adress_country = adress_country;
		this.address_province = address_province;
		this.address_city = address_city;
		this.address_town = address_town;
		this.address_addss = address_addss;
		this.address_pc = address_pc;
		this.address_userid = address_userid;
		this.address_phone = address_phone;
		this.address_username = address_username;
		this.page = page;
		this.limit = limit;
	}
	public Integer getOrder_id() {
		return order_id;
	}
	public void setOrder_id(Integer order_id) {
		this.order_id = order_id;
	}
	public String getOrder_name() {
		return order_name;
	}
	public void setOrder_name(String order_name) {
		this.order_name = order_name;
	}
	public String getOrder_time() {
		return order_time;
	}
	public void setOrder_time(String order_time) {
		this.order_time = order_time;
	}
	public String getOrder_ship() {
		return order_ship;
	}
	public void setOrder_ship(String order_ship) {
		this.order_ship = order_ship;
	}
	public String getOrder_money() {
		return order_money;
	}
	public void setOrder_money(String order_money) {
		this.order_money = order_money;
	}
	public Integer getOrder_product_id() {
		return order_product_id;
	}
	public void setOrder_product_id(Integer order_product_id) {
		this.order_product_id = order_product_id;
	}
	public Integer getOrder_user_id() {
		return order_user_id;
	}
	public void setOrder_user_id(Integer order_user_id) {
		this.order_user_id = order_user_id;
	}
	public Integer getOrder_address_id() {
		return order_address_id;
	}
	public void setOrder_address_id(Integer order_address_id) {
		this.order_address_id = order_address_id;
	}
	public String getOrder_on() {
		return order_on;
	}
	public void setOrder_on(String order_on) {
		this.order_on = order_on;
	}
	public String getOrder_state() {
		return order_state;
	}
	public void setOrder_state(String order_state) {
		this.order_state = order_state;
	}
	public String getOrder_spare1() {
		return order_spare1;
	}
	public void setOrder_spare1(String order_spare1) {
		this.order_spare1 = order_spare1;
	}
	public String getOrder_spare2() {
		return order_spare2;
	}
	public void setOrder_spare2(String order_spare2) {
		this.order_spare2 = order_spare2;
	}
	public String getBegin_order_time() {
		return begin_order_time;
	}
	public void setBegin_order_time(String begin_order_time) {
		this.begin_order_time = begin_order_time;
	}
	public String getEnd_order_time() {
		return end_order_time;
	}
	public void setEnd_order_time(String end_order_time) {
		this.end_order_time = end_order_time;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public Double getProduct_price() {
		return product_price;
	}
	public void setProduct_price(Double product_price) {
		this.product_price = product_price;
	}
	public Double getProduct_discountprice() {
		return product_discountprice;
	}
	public void setProduct_discountprice(Double product_discountprice) {
		this.product_discountprice = product_discountprice;
	}
	public String getProduct_deliveryTime() {
		return product_deliveryTime;
	}
	public void setProduct_deliveryTime(String product_deliveryTime) {
		this.product_deliveryTime = product_deliveryTime;
	}
	public Integer getProduct_id_type() {
		return product_id_type;
	}
	public void setProduct_id_type(Integer product_id_type) {
		this.product_id_type = product_id_type;
	}
	public Integer getProduct_category() {
		return product_category;
	}
	public void setProduct_category(Integer product_category) {
		this.product_category = product_category;
	}
	public Integer getProduct_style() {
		return product_style;
	}
	public void setProduct_style(Integer product_style) {
		this.product_style = product_style;
	}
	public String getProduct_address() {
		return product_address;
	}
	public void setProduct_address(String product_address) {
		this.product_address = product_address;
	}
	public Integer getProduct_count() {
		return product_count;
	}
	public void setProduct_count(Integer product_count) {
		this.product_count = product_count;
	}
	public String getProduct_img() {
		return product_img;
	}
	public void setProduct_img(String product_img) {
		this.product_img = product_img;
	}
	public String getProduct_launch() {
		return product_launch;
	}
	public void setProduct_launch(String product_launch) {
		this.product_launch = product_launch;
	}
	public String getProduct_isPub() {
		return product_isPub;
	}
	public void setProduct_isPub(String product_isPub) {
		this.product_isPub = product_isPub;
	}
	public String getProduct_canUse() {
		return product_canUse;
	}
	public void setProduct_canUse(String product_canUse) {
		this.product_canUse = product_canUse;
	}
	public String getProduct_content() {
		return product_content;
	}
	public void setProduct_content(String product_content) {
		this.product_content = product_content;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_loginname() {
		return user_loginname;
	}
	public void setUser_loginname(String user_loginname) {
		this.user_loginname = user_loginname;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getUser_sex() {
		return user_sex;
	}
	public void setUser_sex(String user_sex) {
		this.user_sex = user_sex;
	}
	public String getUser_birthday() {
		return user_birthday;
	}
	public void setUser_birthday(String user_birthday) {
		this.user_birthday = user_birthday;
	}
	public String getUser_address() {
		return user_address;
	}
	public void setUser_address(String user_address) {
		this.user_address = user_address;
	}
	public String getUser_spare1() {
		return user_spare1;
	}
	public void setUser_spare1(String user_spare1) {
		this.user_spare1 = user_spare1;
	}
	public String getUser_spare2() {
		return user_spare2;
	}
	public void setUser_spare2(String user_spare2) {
		this.user_spare2 = user_spare2;
	}
	public Integer getUser_spare3() {
		return user_spare3;
	}
	public void setUser_spare3(Integer user_spare3) {
		this.user_spare3 = user_spare3;
	}
	public Integer getAddress_id() {
		return address_id;
	}
	public void setAddress_id(Integer address_id) {
		this.address_id = address_id;
	}
	public String getAdress_country() {
		return adress_country;
	}
	public void setAdress_country(String adress_country) {
		this.adress_country = adress_country;
	}
	public String getAddress_province() {
		return address_province;
	}
	public void setAddress_province(String address_province) {
		this.address_province = address_province;
	}
	public String getAddress_city() {
		return address_city;
	}
	public void setAddress_city(String address_city) {
		this.address_city = address_city;
	}
	public String getAddress_town() {
		return address_town;
	}
	public void setAddress_town(String address_town) {
		this.address_town = address_town;
	}
	public String getAddress_addss() {
		return address_addss;
	}
	public void setAddress_addss(String address_addss) {
		this.address_addss = address_addss;
	}
	public String getAddress_pc() {
		return address_pc;
	}
	public void setAddress_pc(String address_pc) {
		this.address_pc = address_pc;
	}
	public Integer getAddress_userid() {
		return address_userid;
	}
	public void setAddress_userid(Integer address_userid) {
		this.address_userid = address_userid;
	}
	public String getAddress_phone() {
		return address_phone;
	}
	public void setAddress_phone(String address_phone) {
		this.address_phone = address_phone;
	}
	public String getAddress_username() {
		return address_username;
	}
	public void setAddress_username(String address_username) {
		this.address_username = address_username;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "Order_View [order_id=" + order_id + ", order_name=" + order_name + ", order_time=" + order_time
				+ ", order_ship=" + order_ship + ", order_money=" + order_money + ", order_product_id="
				+ order_product_id + ", order_user_id=" + order_user_id + ", order_address_id=" + order_address_id
				+ ", order_on=" + order_on + ", order_state=" + order_state + ", order_spare1=" + order_spare1
				+ ", order_spare2=" + order_spare2 + ", begin_order_time=" + begin_order_time + ", end_order_time="
				+ end_order_time + ", product_id=" + product_id + ", product_name=" + product_name + ", product_price="
				+ product_price + ", product_discountprice=" + product_discountprice + ", product_deliveryTime="
				+ product_deliveryTime + ", product_id_type=" + product_id_type + ", product_category="
				+ product_category + ", product_style=" + product_style + ", product_address=" + product_address
				+ ", product_count=" + product_count + ", product_img=" + product_img + ", product_launch="
				+ product_launch + ", product_isPub=" + product_isPub + ", product_canUse=" + product_canUse
				+ ", product_content=" + product_content + ", user_id=" + user_id + ", user_loginname=" + user_loginname
				+ ", user_pwd=" + user_pwd + ", user_sex=" + user_sex + ", user_birthday=" + user_birthday
				+ ", user_address=" + user_address + ", user_spare1=" + user_spare1 + ", user_spare2=" + user_spare2
				+ ", user_spare3=" + user_spare3 + ", address_id=" + address_id + ", adress_country=" + adress_country
				+ ", address_province=" + address_province + ", address_city=" + address_city + ", address_town="
				+ address_town + ", address_addss=" + address_addss + ", address_pc=" + address_pc + ", address_userid="
				+ address_userid + ", address_phone=" + address_phone + ", address_username=" + address_username
				+ ", page=" + page + ", limit=" + limit + "]";
	}
	
	
	
}
