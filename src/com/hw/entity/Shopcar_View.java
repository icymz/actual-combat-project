package com.hw.entity;

public class Shopcar_View {
	private Integer car_id;
	private Integer car_number;
	private Integer car_product_id;
	private Integer car_user_id;
	private String car_spare1;
	private Integer car_spare2;

	private Integer product_id;
	private String product_name;
	private Double product_price;
	private Double product_discountprice;
	private String product_deliveryTime;
	private Integer product_id_type;
	private Integer product_category;
	private Integer product_style;
	private String product_address;
	private Integer product_count;
	private String product_img;
	private String product_launch;
	private String product_isPub;
	private String product_canUse;
	private String product_content;
	private String erro1;
	private String erro2;
	
	private Integer user_id;
	private String user_loginname;
	private String user_pwd;
	private String user_fullname;
	private String user_sex;
	private String user_birthday;
	private String user_address;
	private String user_phone;
	private String user_email;
	private String user_img;
	
	private Integer page;
	private Integer limit;
	public Shopcar_View() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Shopcar_View(Integer car_id, Integer car_number, Integer car_product_id, Integer car_user_id,
			String car_spare1, Integer car_spare2, Integer product_id, String product_name, Double product_price,
			Double product_discountprice, String product_deliveryTime, Integer product_id_type,
			Integer product_category, Integer product_style, String product_address, Integer product_count,
			String product_img, String product_launch, String product_isPub, String product_canUse,
			String product_content, String erro1, String erro2, Integer user_id, String user_loginname, String user_pwd,
			String user_fullname, String user_sex, String user_birthday, String user_address, String user_phone,
			String user_email, String user_img, Integer page, Integer limit) {
		super();
		this.car_id = car_id;
		this.car_number = car_number;
		this.car_product_id = car_product_id;
		this.car_user_id = car_user_id;
		this.car_spare1 = car_spare1;
		this.car_spare2 = car_spare2;
		this.product_id = product_id;
		this.product_name = product_name;
		this.product_price = product_price;
		this.product_discountprice = product_discountprice;
		this.product_deliveryTime = product_deliveryTime;
		this.product_id_type = product_id_type;
		this.product_category = product_category;
		this.product_style = product_style;
		this.product_address = product_address;
		this.product_count = product_count;
		this.product_img = product_img;
		this.product_launch = product_launch;
		this.product_isPub = product_isPub;
		this.product_canUse = product_canUse;
		this.product_content = product_content;
		this.erro1 = erro1;
		this.erro2 = erro2;
		this.user_id = user_id;
		this.user_loginname = user_loginname;
		this.user_pwd = user_pwd;
		this.user_fullname = user_fullname;
		this.user_sex = user_sex;
		this.user_birthday = user_birthday;
		this.user_address = user_address;
		this.user_phone = user_phone;
		this.user_email = user_email;
		this.user_img = user_img;
		this.page = page;
		this.limit = limit;
	}
	public Integer getCar_id() {
		return car_id;
	}
	public void setCar_id(Integer car_id) {
		this.car_id = car_id;
	}
	public Integer getCar_number() {
		return car_number;
	}
	public void setCar_number(Integer car_number) {
		this.car_number = car_number;
	}
	public Integer getCar_product_id() {
		return car_product_id;
	}
	public void setCar_product_id(Integer car_product_id) {
		this.car_product_id = car_product_id;
	}
	public Integer getCar_user_id() {
		return car_user_id;
	}
	public void setCar_user_id(Integer car_user_id) {
		this.car_user_id = car_user_id;
	}
	public String getCar_spare1() {
		return car_spare1;
	}
	public void setCar_spare1(String car_spare1) {
		this.car_spare1 = car_spare1;
	}
	public Integer getCar_spare2() {
		return car_spare2;
	}
	public void setCar_spare2(Integer car_spare2) {
		this.car_spare2 = car_spare2;
	}
	public Integer getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public Double getProduct_price() {
		return product_price;
	}
	public void setProduct_price(Double product_price) {
		this.product_price = product_price;
	}
	public Double getProduct_discountprice() {
		return product_discountprice;
	}
	public void setProduct_discountprice(Double product_discountprice) {
		this.product_discountprice = product_discountprice;
	}
	public String getProduct_deliveryTime() {
		return product_deliveryTime;
	}
	public void setProduct_deliveryTime(String product_deliveryTime) {
		this.product_deliveryTime = product_deliveryTime;
	}
	public Integer getProduct_id_type() {
		return product_id_type;
	}
	public void setProduct_id_type(Integer product_id_type) {
		this.product_id_type = product_id_type;
	}
	public Integer getProduct_category() {
		return product_category;
	}
	public void setProduct_category(Integer product_category) {
		this.product_category = product_category;
	}
	public Integer getProduct_style() {
		return product_style;
	}
	public void setProduct_style(Integer product_style) {
		this.product_style = product_style;
	}
	public String getProduct_address() {
		return product_address;
	}
	public void setProduct_address(String product_address) {
		this.product_address = product_address;
	}
	public Integer getProduct_count() {
		return product_count;
	}
	public void setProduct_count(Integer product_count) {
		this.product_count = product_count;
	}
	public String getProduct_img() {
		return product_img;
	}
	public void setProduct_img(String product_img) {
		this.product_img = product_img;
	}
	public String getProduct_launch() {
		return product_launch;
	}
	public void setProduct_launch(String product_launch) {
		this.product_launch = product_launch;
	}
	public String getProduct_isPub() {
		return product_isPub;
	}
	public void setProduct_isPub(String product_isPub) {
		this.product_isPub = product_isPub;
	}
	public String getProduct_canUse() {
		return product_canUse;
	}
	public void setProduct_canUse(String product_canUse) {
		this.product_canUse = product_canUse;
	}
	public String getProduct_content() {
		return product_content;
	}
	public void setProduct_content(String product_content) {
		this.product_content = product_content;
	}
	public String getErro1() {
		return erro1;
	}
	public void setErro1(String erro1) {
		this.erro1 = erro1;
	}
	public String getErro2() {
		return erro2;
	}
	public void setErro2(String erro2) {
		this.erro2 = erro2;
	}
	public Integer getUser_id() {
		return user_id;
	}
	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_loginname() {
		return user_loginname;
	}
	public void setUser_loginname(String user_loginname) {
		this.user_loginname = user_loginname;
	}
	public String getUser_pwd() {
		return user_pwd;
	}
	public void setUser_pwd(String user_pwd) {
		this.user_pwd = user_pwd;
	}
	public String getUser_fullname() {
		return user_fullname;
	}
	public void setUser_fullname(String user_fullname) {
		this.user_fullname = user_fullname;
	}
	public String getUser_sex() {
		return user_sex;
	}
	public void setUser_sex(String user_sex) {
		this.user_sex = user_sex;
	}
	public String getUser_birthday() {
		return user_birthday;
	}
	public void setUser_birthday(String user_birthday) {
		this.user_birthday = user_birthday;
	}
	public String getUser_address() {
		return user_address;
	}
	public void setUser_address(String user_address) {
		this.user_address = user_address;
	}
	public String getUser_phone() {
		return user_phone;
	}
	public void setUser_phone(String user_phone) {
		this.user_phone = user_phone;
	}
	public String getUser_email() {
		return user_email;
	}
	public void setUser_email(String user_email) {
		this.user_email = user_email;
	}
	public String getUser_img() {
		return user_img;
	}
	public void setUser_img(String user_img) {
		this.user_img = user_img;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	@Override
	public String toString() {
		return "Shopcar_View [car_id=" + car_id + ", car_number=" + car_number + ", car_product_id=" + car_product_id
				+ ", car_user_id=" + car_user_id + ", car_spare1=" + car_spare1 + ", car_spare2=" + car_spare2
				+ ", product_id=" + product_id + ", product_name=" + product_name + ", product_price=" + product_price
				+ ", product_discountprice=" + product_discountprice + ", product_deliveryTime=" + product_deliveryTime
				+ ", product_id_type=" + product_id_type + ", product_category=" + product_category + ", product_style="
				+ product_style + ", product_address=" + product_address + ", product_count=" + product_count
				+ ", product_img=" + product_img + ", product_launch=" + product_launch + ", product_isPub="
				+ product_isPub + ", product_canUse=" + product_canUse + ", product_content=" + product_content
				+ ", erro1=" + erro1 + ", erro2=" + erro2 + ", user_id=" + user_id + ", user_loginname="
				+ user_loginname + ", user_pwd=" + user_pwd + ", user_fullname=" + user_fullname + ", user_sex="
				+ user_sex + ", user_birthday=" + user_birthday + ", user_address=" + user_address + ", user_phone="
				+ user_phone + ", user_email=" + user_email + ", user_img=" + user_img + ", page=" + page + ", limit="
				+ limit + "]";
	}
	
	
	
}
